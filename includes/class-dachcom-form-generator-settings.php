<?php

/**
 * Get Form Generator Core Settings and Options
 *
 * Maybe we have to move this file to /admin, if no frontEnd Settings will come.
 *
 * @since      1.0.0
 * @package    DachcomFormGenerator
 * @subpackage DachcomFormGenerator/includes
 * @author     Stefan Hagspiel <shagspiel@dachcom.com>
 */
class DachcomFormGenerator_Settings {

    private $usingThemosis = FALSE;

	private $wp_editor_settings = array();

	private $form_config_file = '';

	private $countries_config_file = '';

	public function __construct()
	{


        if( class_exists('THFWK_Themosis') ) {

            $this->usingThemosis = TRUE;

        }

		//define wp_editor
		$this->wp_editor_settings = array(
			'wpautop' => true,
			'media_buttons' => false,
			'textarea_rows' => 18,
			'tabindex' => '',
			'tabfocus_elements' => ':prev,:next',
			'editor_css' => '',
			'editor_class' => '',
			'teeny' => false,
			'dfw' => false,
			'quicktags' => true,
			'tinymce' => array(
				'theme_advanced_statusbar_location' => 'bottom',
				'theme_advanced_path' => false,
				'theme_advanced_resizing' => true,
				'theme_advanced_resize_horizontal' => false,
				'theme_advanced_buttons1' => 'bold,italic,underline,justifyleft,justifycenter,justifyright,spellchecker,link,unlink,undo,redo',
				'theme_advanced_buttons2' => ''
			),

		);

        if( !$this->usingThemosis ) {

            $this->form_config_file = get_template_directory() . '/config/form.config.php';


        } else {

            $this->form_config_file = get_template_directory() . '/resources/config/form.config.php';

        }

		//countrylist
		$this->countries_config_file =  plugin_dir_path( __FILE__ ) . 'config/countries.config.php';

	}

	function get_wp_editor_settings() {

		return $this->wp_editor_settings;
	}

	function get_config_form() {

		if( !file_exists( $this->form_config_file ) ) {

            // @fixme: add admin error notice!
			//wp_die( 'cannot load "' . $this->form_config_file . '". file not found');
            return false;
		}

		/**
		 * @var $form_config
		 */
        $form_config = require( $this->form_config_file );

		return $form_config;

	}

	function get_countries_list( $language_code = 'de_DE', $value = null) {

		if( !file_exists( $this->countries_config_file ) ) {

			wp_die( 'cannot load "' . $this->countries_config_file . '". file not found');
		}

		/**
		 * @var $country_list;
		 */
        $country_list = require( $this->countries_config_file );

        $fallback_lang_code = 'en_US';

        if( isset( $country_list[ $language_code ] ) )
            $language_list = $country_list[ $language_code ];
        else
            $language_list = $country_list[ $fallback_lang_code ];

		if( !is_null( $value ) )
            return $language_list[ $value ];

        return $language_list;


	}

	function get_current_languagecode( $LCID = FALSE ) {

        global $blog_id;

        $locale = get_locale();
        $lang_default = explode('_', $locale);

        if( !is_multisite() ) {

			if( $LCID ) {

                return $locale;

            }  else {

                return $lang_default[0];

            }

		}

		$blog_info = get_blog_details( $blog_id );

		$blog_path = $blog_info->path;
		$lang_code = str_replace('/', '', $blog_path);

		if(empty( $lang_code) )
			$tag = $lang_default[0];
		else
			$tag = $lang_code;


		if( $LCID ) {
			if( $tag == 'en')
				$tag = $tag . '-US';
			else
				$tag = $tag .'-'.strtoupper( $tag );
		}

		return $tag;

	}

	function get_form_configuration_data() {

		/**
		 * type = textfield|textarea
		 * show_user = dont show user to default user, only admin (dachcom)
		 * aPh = available placeholder. eg: %form_name% will be replaced before mail submit
		 * rewriteable = is it possible to edit in backend?
		 *
		 */

		$recipient_help = 'Empfänger dieses Formulars (es können mehrere Adressen kommasepariert angegeben werden)';

		$text_on_success = 'Text, welcher unterhalb des Formulares erscheinen soll, sobald dieses erfolgreich versendet wurde.';
        $text_on_success_mail = 'Inhaltstext für das Admin-Mail. Dieses Mail erhält der Administrator.';

		return array(

            'form_name'					=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Name', 'help' => '', 'default' => '' ),
            'form_type'					=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,  'show_user' => TRUE,  'title' => 'Form Typ', 'help' => 'Ändern Sie den Wert auf "inline", um eine Inline-Form zu erstellen.', 'default' => '' ),
			'required_symbol' 			=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Pflichtfeld-Symbol', 'help' => '', 'default' => '*' ),
			'description' 				=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Beschhreibung', 'help' => '', 'default' => '' ),

			'required_text' 			=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Pflichtfeld-Text', 'help' => '', 'default' => '' ),
			'file_management'			=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Dateimanagement', 'help' => '', 'default' => 'server_link' ),

			'appear_in'					=> array('type'=> 'textfield', 'awaiting' => 'array',  'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Erscheinen', 'help' => '', 'default' => array() ),
			'enctype'					=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Enctype', 'help' => '', 'default' => '' ),

			'recipient'					=> array('type'=> 'textfield', 'awaiting' => 'array',  'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Empfänger', 'help' => $recipient_help, 'default' => '' ),

			'show_reset_button' 		=> array('type'=> 'textfield', 'awaiting' => 'bool',   'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Reset Button anzeigen', 'help' => '', 'default' => TRUE ),
			'show_response_field' 		=> array('type'=> 'textfield', 'awaiting' => 'bool',   'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Response Feld anzeigen', 'help' => '', 'default' => TRUE ),

			'interface_submit_button'	=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,	'show_user' => FALSE, 'title' => 'Submit-Button Text', 'help' => '', 'default' => 'Formular Absenden' ),
			'interface_reset_button'	=> array('type'=> 'textfield', 'awaiting' => 'string', 'rewriteable' => FALSE,	'show_user' => FALSE, 'title' => 'Reset-Button Text', 'help' => '', 'default' => 'zurücksetzen' ),

			'send_mail'					=> array('type'=> 'textfield', 'awaiting' => 'bool',   'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Mail versenden', 'help' => 'Deaktivieren Sie diese Option, wenn keine Mails versendet werden sollen.', 'default' => FALSE ),
			'save_data_in_db'			=> array('type'=> 'textfield', 'awaiting' => 'bool',   'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Daten in Datenbank speichern', 'help' => '', 'default' => TRUE ),

            'is_newsletter_form'		=> array('type'=> 'textfield', 'awaiting' => 'bool',   'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Newsletter', 'help' => 'Wenn sich das Formular um eine Newsletter-Anmeldung handelt, hier definieren. Das <em>Field</em>-Array muss ein Feld mit dem name "newsletter_fields" enthalten. Dieses kann entweder ein hidden, checkbox, oder radio Element sein.', 'default' => FALSE ),
            'newsletter_landingpage_id' => array('type'=> 'textfield', 'awaiting' => 'number', 'rewriteable' => TRUE,   'show_user' => TRUE,  'title' => 'Newsletter Landing PageID', 'help' => 'Geben Sie ein PageID ein, auf welcher die User nach Bearbeitung einer Newsletter-Einstellung geleitet werden sollen.', 'default' => FALSE ),

            'newsletter_doi_subject'   	=> array('type'=> 'textfield', 'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => TRUE,  'title' => 'Newsletter-Bestätigung Betreff (Double Opt-In)', 'help' => 'Betreff an User um Newsletter zu bestätigen', 'default' => '' ),
            'newsletter_doi_text'   	=> array('type'=> 'textarea',  'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => TRUE,  'title' => 'Newsletter-Bestätigung Text (Double Opt-In)', 'help' => 'Inhaltstext an User um Newsletter zu bestätigen', 'aPh' => '%activation_link% %selected_newsletters%', 'default' => '' ),
            'newsletter_success_subject'=> array('type'=> 'textfield', 'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => TRUE,  'title' => 'Newsletter Abschluss Betreff', 'help' => 'Betreff, wenn Newsletter erfolgreich aktiviert wurde', 'default' => '' ),
            'newsletter_success_text'	=> array('type'=> 'textarea',  'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => TRUE,  'title' => 'Newsletter Abschluss Text', 'help' => 'Text, wenn Newsletter erfolgreich aktiviert wurde', 'aPh' => '%selected_newsletters%', 'default' => '' ),
			'newsletter_success_admin'	=> array('type'=> 'textarea',  'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => TRUE,  'title' => 'Newsletter Admin Text-Bestätigung an Admin', 'help' => 'Newsletter Text-Bestätigung an Admin, wenn User sich erfolgreich angemeldet hat', 'default' => '' ),

			'send_copy_to_user'	        => array('type'=> 'textfield', 'awaiting' => 'bool',   'rewriteable' => FALSE,  'show_user' => FALSE, 'title' => 'Kopie an Benutzer versenden?', 'help' => '', 'default' => FALSE ),

			'copy_subject'		        => array('type'=> 'textfield', 'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => FALSE, 'title' => 'Kopie Betreff', 'help' => '', 'default' => FALSE ),
			'copy_text'			        => array('type'=> 'textarea',  'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => FALSE, 'title' => 'Kopie Text', 'help' => '', 'aPh' => '%rendered_form_data%', 'default' => FALSE ),

			'text_on_success'			=> array('type'=> 'textarea',  'awaiting' => 'string', 'rewriteable' => TRUE, 	'show_user' => FALSE, 'title' => 'Bestätigungstext', 'help' => $text_on_success, 'aPh' => '%email%', 'default' => '' ),
			'default_mail_text'			=> array('type'=> 'textarea',  'awaiting' => 'text',   'rewriteable' => TRUE,   'show_user' => FALSE, 'title' => 'Bestätigungstext Mail', 'help' => $text_on_success_mail, 'aPh' => '%form_name% %rendered_form_data%', 'default' => 'Hallo, es wurde ein Formular (%form_name%) versendet' ),

		);

	}

}
