<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/includes
 * @author     Dachcom Digital <shagspiel@dachcom.com>
 */
class Dachcom_Form_Generator_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {

        global $wpdb;

        if (!class_exists('DachcomPluginObserver')) {

            wp_die('Plugins, welche vom <a href="http://dachcom-digital.ch">Dachcom Digital Team</a> entwickelt wurden, benötigen das "Dachcom Plugin Observer" MU-Plugin. <a href="' . admin_url('plugins.php') . '">Zur&uuml;ck</a>.');
            return;

        }

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $table_name_core = $wpdb->prefix . 'dcd_form_generator';

        if ($wpdb->get_var("SHOW TABLES LIKE '" . $table_name_core . "'") != $table_name_core) {

            $sql = 'CREATE TABLE IF NOT EXISTS ' . $table_name_core . ' (
				  `ID` int(10) NOT NULL AUTO_INCREMENT,
				  `formID` varchar(100) NOT NULL,
				  `form_key` varchar(100) NOT NULL,
				  `form_value` longtext NOT NULL,
				  `form_type` varchar(100) NOT NULL,
				  PRIMARY KEY (`ID`)
				) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;';

            $wpdb->query($sql);

        }

        $table_name_data = $wpdb->prefix . "dcd_form_generator_data";

        if ($wpdb->get_var("SHOW TABLES LIKE '" . $table_name_data . "'") != $table_name_data) {

            $sql = "CREATE TABLE IF NOT EXISTS `$table_name_data` (
				`id` mediumint(9) NOT NULL AUTO_INCREMENT,
				`formID` varchar(200) NOT NULL,
				`recipient` varchar(200) NOT NULL,
				`formdata` text NOT NULL,
				`nl_data` text,
				`stamp` datetime NOT NULL,
				PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";

            $wpdb->query($sql);

        }

    }

}
