<?php

/**
 *
 * @since      1.0.0
 * @package    DachcomFormGenerator
 * @subpackage DachcomFormGenerator/shortcodes
 * @author     Stefan Hagspiel <shagspiel@dachcom.com>
 */
class DachcomFormGenerator_ShortCodes{

    /**
     * @var DachcomFormGenerator_Settings
     */
    var $settings = null;

    /**
     * Core Shortcode Settings
     * @var array
     */
    var $allowed_shortcodes = array();

	public function __construct( $settings )
	{

        $this->settings = $settings;

        $this->register_shortcode_methods();

	}

    public function register_shortcode_methods() {

        // add core shortcodes
        $this->allowed_shortcodes = array(

            'country' => array($this, 'parse_shortcode_country'),
            'city' => array($this, 'parse_shortcode_city')

        );

        $user_funcs = apply_filters( 'dachcom-form-generator/register-shortcode-methods', array() );

        //if user added core shortcodes, remove them!!
        unset( $user_funcs['country'], $user_funcs['city'] );

        $this->allowed_shortcodes = array_merge( $this->allowed_shortcodes, $user_funcs);

    }

    public function parse_shortcodes( $value ) {

        $allowed_tags = $this->get_valid_shortcode_methods();

        $pattern = '\[(\[?)('.implode('|',$allowed_tags).')(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)';

        if ( preg_match_all( '/'.$pattern.'/s', $value, $matches )
            && array_key_exists( 2, $matches ) )
        {

            $code = $matches[2][0];

            //generates params and add code
            $_params = isset($matches[3][0]) ? $matches[3][0]: '';
            $dat= explode(" ", $_params);
            $params = array();
            foreach ($dat as $d){
                if( empty($d) ) continue;
                list($opt, $val) = explode("=", $d);
                $params[$opt] = trim($val, '"');
            }

            $value = $this->parse($code, $params);

        }

        return $value;

    }

    private function parse( $type, $params ) {

        if( !isset( $this->allowed_shortcodes[ $type ]))
            return false;

        if( is_array( $this->allowed_shortcodes[ $type ] ) &&
            method_exists($this->allowed_shortcodes[ $type ][0], $this->allowed_shortcodes[ $type ][1] ) )
            return call_user_func_array($this->allowed_shortcodes[ $type ], $params );
        else if( is_string($this->allowed_shortcodes[ $type ]) && function_exists( $this->allowed_shortcodes[ $type ] ))
            return call_user_func( $this->allowed_shortcodes[ $type ], $params );

        return false;

    }

    private function get_valid_shortcode_methods() {

        return array_keys( $this->allowed_shortcodes );

    }

    /**
     * Get Country List
     * @param $atts
     * @return mixed
     */
    private function parse_shortcode_country( $atts = array() ) {

        $atts = shortcode_atts( array(
            'lang' => '',
        ), $atts, 'country' );

        return $this->settings->get_countries_list( $this->settings->get_current_languagecode( TRUE ) );

    }

    /**
     *
     * Gets related city to given country.
     * @not_implmentend
     */
    private function parse_shortcode_city( $atts = array() ) {

        $atts = shortcode_atts( array(
            'country' => '',
        ), $atts, 'city' );

        return $this->settings->get_city_list( $this->settings->get_current_languagecode( TRUE ) );

    }

}