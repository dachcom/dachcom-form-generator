<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/includes
 * @author     Dachcom Digital <shagspiel@dachcom.com>
 */
class Dachcom_Form_Generator_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
