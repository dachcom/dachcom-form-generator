<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/includes
 * @author     Dachcom Digital <shagspiel@dachcom.com>
 */
class Dachcom_Form_Generator {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Dachcom_Form_Generator_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'dachcom-form-generator';
		$this->version = '2.3.3';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Dachcom_Form_Generator_Loader. Orchestrates the hooks of the plugin.
	 * - Dachcom_Form_Generator_i18n. Defines internationalization functionality.
	 * - Dachcom_Form_Generator_Admin. Defines all hooks for the admin area.
	 * - Dachcom_Form_Generator_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dachcom-form-generator-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dachcom-form-generator-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-dachcom-form-generator-admin.php';

		if( is_admin() ) {

			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/fields/form-field.php';
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/fields/form-field-config.php';
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/fields/form-field-element.php';

		}

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/vendor/flex-archive.class.php';


        /**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-public.php';

		/**
		 * Load the Public Functions (API)
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-functions.php';

        /**
         * Get the form settings Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dachcom-form-generator-settings.php';

        /**
         * Get the form default shortcodes Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-dachcom-form-generator-shortcodes.php';

        /**
         * Get the storage Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-storage.php';

        /**
         * Get the attachment Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-attachment.php';

        /**
         * Get the mailer Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-mailer.php';

        /**
         * Get the structure Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-structure.php';

        /**
         * Get the mapper Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-mapper.php';

        /**
         * Get the newsletter Class
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-newsletter.php';

		/**
		 * Get the ajax Class
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-ajax.php';

		/**
		 * get all Form Element Classes
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-text.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-number.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-email.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-password.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-radio.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-checkbox.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-hidden.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-input-file.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-select.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/form-element-textarea.php';

		/**
		 * This is the Builder.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-dachcom-form-generator-builder.php';

		$this->loader = new Dachcom_Form_Generator_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Dachcom_Form_Generator_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Dachcom_Form_Generator_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Dachcom_Form_Generator_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'init', 					$plugin_admin, 'register_plugin' );
		$this->loader->add_action( 'admin_init',			$plugin_admin, 'plugin_core_settings_api_init' );

		$this->loader->add_action( 'admin_menu', 			$plugin_admin, 'setup_backend_menu' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Dachcom_Form_Generator_Public( $this->get_plugin_name(), $this->get_version() );
		$ajax_handler = new DachcomFormBuilder_Ajax();

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Dachcom_Form_Generator_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
