<?php

/**
 * Adds a config table for the given form
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/admin/partials
 */


$ignore_array = array(

    'show_reset_button',
    'show_response_field',
    'required_text',
    'required_symbol',
    'required_text',
    'deactivate_mail_send',
    'file_management',
    'save_data_in_db',
    'send_confirmation_to_user',
    'description',
    'enctype',
    'appear_in'
);

//hide this setting when user is not support_team
$ignore_array_but_not_for_mrs = array();

if (!$this->is_support_team) {

    $ignore_array = array_merge($ignore_array, $ignore_array_but_not_for_mrs);
    $ignore_array = array_unique($ignore_array);

}

$formID = $_GET['page'];

$formVals = $this->get_form_vals($formID);

?>

<h4>Allgemeine Konfiguration</h4>

<table class="form-table mrs_form_field">

    <?php

    foreach ($configurations as $key => $val) {

        if (in_array($key, $ignore_array))
            continue;

        //special treatment (array for newsletter-stuff [ activation_form ] )
        if ($key == 'activation_form') {

            foreach ($val as $newsletter_key => $newsletter_val) {

                $type = $newsletter_val['type'];
                $fieldName = $newsletter_key;
                $be_title = isset($newsletter_val['be_title']) ? $newsletter_val['be_title'] : $fieldName;
                $explanation = isset($newsletter_val['explanation']) ? '<p class="description">' . $newsletter_val['explanation'] . '</p>' : '';
                $value = isset($formVals[$fieldName]) ? $formVals[$fieldName]->form_value : '';

                switch ($type) {

                    case 'text':

                        ?>

                        <tr valign="top">

                            <th scope="row">
                                <label for=""><?php echo $be_title; ?></label><br><br>
                                <?php $this->render_placeholders($newsletter_val['value']); ?>
                                <br><br>
                            </th>

                            <td>
                                <?php echo $explanation; ?>
                                <input style="width:100%;" name="<?php echo $fieldName; ?>" id="<?php echo $fieldName; ?>" type="<?php echo $type; ?>" value="<?php echo $value; ?>"/>
                            </td>

                        </tr>

                        <?php
                        break;

                    case 'textarea':

                        ?>

                        <tr valign="top">

                            <th scope="row">

                                <label for=""><?php echo $newsletter_key; ?></label><br><br>
                                <?php $this->render_placeholders($newsletter_val['value']); ?>
                                <br><br>

                            </th>

                            <td>

                                <?php

                                echo $explanation;
                                $editorKey = $newsletter_key;
                                wp_editor( $value, $editorKey, $this->wp_editor_settings );

                                ?>

                            </td>

                        </tr>

                        <?php
                        break;

                    default:

                        break;


                }

            }

        } else {

            $type = $val['type'];

            $fieldName = $key;
            $be_title = isset($val['be_title']) ? $val['be_title'] : $fieldName;
            $explanation = isset($val['explanation']) ? '<p class="description">' . $val['explanation'] . '</p>' : '';
            $value = isset($formVals[$fieldName]) ? $formVals[$fieldName]->form_value : '';

            switch ($type) {

                case 'text':

                    ?>

                    <tr valign="top">

                        <th scope="row">
                            <label for=""><?php echo $be_title; ?></label>

                            <?php echo $this->render_placeholders($val['value']); ?>
                            <br><br>
                        </th>

                        <td>
                            <?php echo $explanation; ?>
                            <input style="width:100%;" name="<?php echo $fieldName; ?>" id="<?php echo $fieldName; ?>"
                                   type="<?php echo $val['type']; ?>" value="<?php echo $value; ?>"/>
                        </td>

                    </tr>

                    <?php

                    break;

                case 'textarea':

                    ?>

                    <tr valign="top">

                        <th scope="row">
                            <label for=""><?php echo $be_title; ?></label><br><br>
                            <?php $this->render_placeholders($val['value']); ?>
                            <br><br>
                        </th>

                        <td>

                            <?php

                            echo $explanation;
                            $editorKey = $fieldName;
                            wp_editor($value, $editorKey, $this->settings->get_wp_editor_settings());

                            ?>

                        </td>

                    </tr>

                    <?php break;

                case 'checkbox':

                    $checkedForDefaultInput = $val['value'] === TRUE ? 'checked="checked"' : '';

                    ?>

                    <tr valign="top">

                        <th scope="row"><label for=""><?php echo $be_title; ?></label></th>

                        <td>
                            <?php echo $explanation; ?>
                            <input type="checkbox" name="<?php echo $key; ?>" value="<?php echo $key; ?>">
                        </td>

                    </tr>

                    <?php

                    break;

                default:

                    break;

            }

            ?>

            <tr class="spacer">
                <th scope="row"></th>
                <td></td>
            </tr>

        <?php

        }

    }

    ?>
</table>