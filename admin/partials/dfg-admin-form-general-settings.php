<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/admin/partials
 */

$generally_settings = array(

    'mail_signature' => array(

        'type' => 'textarea',
        'be_title' => 'Mail Signatur',
        'explanation' => 'Dieser Text erscheint in allen E-Mails als Signatur')

);

$formVals = $this->get_form_vals('dfg-default_settings');

?>

<h3>Einstellungen: Allgemeines</h3>

<table class="form-table mrs_form_field">

    <?php

    foreach ($generally_settings as $key => $gs) {

        $type = $gs['type'];
        $be_title = isset($gs['be_title']) ? $gs['be_title'] : $key;
        $explanation = isset($gs['explanation']) ? '<p class="description">' . $gs['explanation'] . '</p>' : '';
        $value = isset($formVals[$key]) ? $formVals[$key]->form_value : '';

        switch ($type) {

            case 'text':

                ?>

                <tr valign="top">

                    <th scope="row">
                        <label for=""><?php echo $be_title; ?></label>
                    </th>

                    <td>
                        <?php echo $explanation; ?>
                        <input style="width:100%;" name="generally[<?php echo $key; ?>]" id="<?php echo $key; ?>'" type="<?php echo $gs['type']; ?>" value="<?php echo $value; ?>"/>
                    </td>

                </tr>

                <?php

                break;

            case 'textarea':

                ?>

                <tr valign="top">

                    <th scope="row">
                        <label for=""><?php echo $be_title; ?></label><br><br>
                    </th>

                    <td>
                        <?php

                        echo $explanation;

                        $value = html_entity_decode(stripcslashes($value));

                        $wp_settings = $this->settings->get_wp_editor_settings();
                        $wp_settings['textarea_name'] = 'generally[' . $key . ']';
                        wp_editor($value, $key, $wp_settings );

                        ?>

                    </td>

                </tr>

                <?php

                break;


            default:

                break;
        }

        ?>

        <tr class="spacer">
            <th scope="row"></th>
            <td></td>
        </tr>

    <?php } # foreach ?>

</table>