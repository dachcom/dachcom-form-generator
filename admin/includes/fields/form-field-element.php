<?php

class FormField_Element
{

    /**
     * stores the data from the theme/config/form.config.php data
     * @var array
     */
    var $config_data = array();

    /**
     * stores the data from the theme/config/form.config.php, overwritten
     * @var array
     */
    var $db_data = array();

    /**
     * if is_admin is false, config fields wont be shown
     * @var bool
     */
    var $is_admin = FALSE;

    /**
     * ignored fields, not editable in backend
     * @var array
     */
    var $ignore_fields_array = array();

    /**
     * constructor
     */
    function __construct() { }

    function set_data( $config_data )
    {

        $this->ignore_fields_array = array('seperator', 'headline', 'custom_html', 'row-start', 'row-end', 'col-start', 'col-end', 'column', 'image', 'repeater', 'required_block', 'submit_block');

        $this->config_data = $config_data;

    }

    function set_db_data( $db_data ) {

        $this->db_data = $db_data;
    }

    function parse()
    {

        $str = $this->create_table_header();

        $renderElEngine = FormField::getInstance();
        $renderElEngine::set_el_name_prefix( 'element' );

        foreach ($this->config_data as $config_el_name => $config_data) {

            if( !isset( $config_data['type'] ) )
                continue;

            if (in_array($config_data['type'], $this->ignore_fields_array ) )
                continue;

            $info = $this->get_config_field_info($config_el_name . '_title_mail', $config_data);
            $info_prefix = $this->get_config_field_info($config_el_name . '_title_mail_prefix', $config_data);
            $info_suffix = $this->get_config_field_info($config_el_name . '_title_mail_suffix', $config_data);

            //print_r($info);

            $el = $renderElEngine::render( $info);
            $el_prefix = $renderElEngine::render( $info_prefix );
            $el_suffix = $renderElEngine::render( $info_suffix );

            $str .= $this->create_table_row( $el, $el_prefix, $el_suffix, $info );

        }

        $str .= $this->create_table_footer();


        return $str;

    }

    private function get_config_field_info($config_el_name, $config_data)
    {

        $label = $config_el_name;

        if( isset ( $config_data['title'] ) && !empty( $config_data['title'] ) )
            $label = $config_data['title'];

        $label .= ' (<code>' . $config_data['type'] . '</code>)';


        $info = array(
            'type' => 'textfield',
            'title' => $label,
            'value' => $this->get_element_value( $config_el_name ),
            'name' => $config_el_name
        );

        return $info;
    }

    private function get_element_value( $config_el_name ) {

        if( isset( $this->db_data[ $config_el_name ]))
            return $this->db_data[ $config_el_name ]->form_value;

        return '';

    }

    private function create_table_row( $el, $el_prefix, $el_suffix, $info ) {

        $str = '<tr valign="top">

                <th scope="row"><label for="">Titel</label></th>

                <td>
                    <p class="description">' . $info['title'] . '</p>
                </td>

            </tr>

             <tr valign="top">

                <th scope="row">
                    <label for="">Mail-Titel</label>
                </th>

                <td>
                    ' . $el . '
                </td>

            </tr>

            <tr valign="top">
                <th scope="row">
                  <label for="">Prefix</label> <i class="small"><small><br>HTML-Daten, vor der Wertausgabe</small></i>
                </th>
                <td>
                    ' . $el_prefix . '
                </td>
            </tr>

             <tr valign="top">
                <th scope="row">
                  <label for="">Suffix</label> <i class="small"><small><br>HTML-Daten, nach der Wertausgabe</small></i>
                </th>
                <td>
                    ' . $el_suffix . '
                </td>
            </tr>

            <tr class="spacer">
                <th scope="row"></th>
                <td></td>
            </tr>';

        return $str;

    }

    private function create_table_header() {

        return '<h4>Einstellung einzelner Felder</h4>

                    <p>Wird kein <b>Mail-Titel</b> definiert, so wird der Standardwert von <b>Titel</b> in der Mail angezeigt!
                    <br>Für einen <b>Umbruch</b> nach einem Wert, geben Sie bitte folgendes ein: <code>' . htmlspecialchars('<br>') . '</code></p>

                    <table class="form-table dachcom_form_generator">';

    }

    private function create_table_footer() {

        return '<tr class="spacer">
                    <th scope="row"></th>
                    <td></td>
                </tr>
            </table>';

    }


}