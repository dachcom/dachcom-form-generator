<?php

class FormField
{

    static $el_name_prefix;

    public static function getInstance()
    {

        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;

    }

    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }

    public static function set_el_name_prefix( $prefix ) {

        self::$el_name_prefix = $prefix;

    }

    public static function render( $el_data ) {

        $el_data_method = 'render' . ucfirst( $el_data['type'] );

        if( method_exists(self::getInstance(), $el_data_method ) ) {

            return self::$el_data_method( $el_data );

        }

        return '[render' . ucfirst( $el_data['type'] ) . '() not found]';

    }

    private static function renderTextfield( $data ) {

        $value = is_array( $data['value'] ) ? implode(',', $data['value'] ) : $data['value'];

        $str = '<input type="text" name="' . self::generateElementFormName( $data['name'] ) . '" style="width:100%;" value="' . $value. '" ' . self::generateElementPlaceholder($data) . ' ' . self::generateElementState($data) . ' />';

        return $str;
    }


    private static function renderTextArea( $data ) {

        $settings = new DachcomFormGenerator_Settings();

        ob_start();

        $wp_settings = $settings->get_wp_editor_settings();

        $wp_settings['textarea_name'] = self::generateElementFormName( $data['name'] );

        wp_editor( $data['value'], $data['name'], $wp_settings);

        $str = ob_get_contents();

        ob_get_clean();

        return $str;
    }

    private static function renderCheckbox( $data ) {

        $str = '<input type="checkbox" name="' . self::generateElementFormName( $data['name'] ) . '" value="' . $data['value'] . '" ' . self::generateElementState($data) . ' />';

        return $str;

    }

    private static function generateElementFormName( $name ) {

        if( empty( self::$el_name_prefix ) )
            return $name;
        else
            return self::$el_name_prefix . '[' . $name . ']';

    }


    private static function generateElementPlaceholder( $data ) {
        return isset( $data['placeholder'] ) && $data['placeholder'] != '' ? 'placeholder="'.$data['placeholder'] .'"' : '';

    }

    private static function generateElementState( $data ) {
        return isset( $data['disabled'] ) && $data['disabled'] == TRUE ? 'disabled="disabled"' : '';

    }
}