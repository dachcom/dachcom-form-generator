<?php

class FormField_Config
{

    /**
     *
     * stores the default config data, setted in DachcomFormGeneratorSettings
     * @var array
     */
    var $default_config_data = array();

    /**
     * stores the data from the theme/config/form.config.php data
     * @var array
     */
    var $config_data = array();

    /**
     * stores the data from the theme/config/form.config.php, overwritten
     * @var array
     */
    var $db_data = array();

    /**
     * if is_admin is false, config fields wont be shown
     * @var bool
     */
    var $is_admin = FALSE;

    function __construct()
    {

    }

    function set_data($config_data)
    {

        $this->config_data = $config_data;

    }

    function set_db_data( $db_data ) {

        $this->db_data = $db_data;
    }

    function set_default_config_data($default_config_data)
    {

        $this->default_config_data = $default_config_data;

    }

    function set_admin($state)
    {

        $this->is_admin = $state;

    }

    function parse()
    {

        $str = $this->create_table_header();

        $renderElEngine = FormField::getInstance();
        $renderElEngine::set_el_name_prefix( 'config' );

        foreach ($this->config_data as $config_el_name => $config_value) {

            $info = $this->get_config_field_info($config_el_name);

            if ($info === FALSE)
                continue;

            $aPh = '';

            if( isset( $info['aPh'] ) )
                $aPh = $this->render_placeholders( $info['aPh'] );

            $str .= $this->create_table_row( $info['title'], $info['help'], $renderElEngine::render( $info ), $aPh );

        }

        $str .= $this->create_table_footer();

        return $str;

    }

    private function get_config_field_info($config_el_name)
    {
        if (!isset($this->default_config_data[$config_el_name]))
            return FALSE;

        $info = $this->default_config_data[$config_el_name];
        $info['name'] = $config_el_name;

        if( $info['rewriteable'] == FALSE)
            $info['disabled'] = TRUE;

        $value = $this->get_element_value( $config_el_name );

        if( empty( $value ) ) {

            $cast_type = $info['awaiting'];
            $default_value = $this->config_data[ $config_el_name ];

            if ($cast_type == 'string')
                $value = (string)$default_value;
            elseif ($cast_type == 'array')
                $value = (array)$default_value;
            elseif ($cast_type == 'bool')
                $value = (bool)$default_value;

        }

        $info['value'] = $value;

        return $info;

    }

    private function get_element_value( $config_el_name ) {

        if( isset( $this->db_data[ $config_el_name ]))
            return $this->db_data[ $config_el_name ]->form_value;

        return '';

    }

    private function render_placeholders( $aPh ){

        preg_match_all("'%(.*?)%'si",$aPh, $matches);

        $str = '';

        if( !empty($matches[0]) ) {

            $str .= '<hr>Verfügbare Platzhalter:<br>';

            foreach($matches[0] as $match) {

                $str .= '<code>' . $match . ' </code>';

            }

        }

        return $str;

    }

    private function create_table_header() {

        return '<h4>Allgemeine Konfiguration</h4>
                    <table class="form-table dachcom_form_generator">';

    }

    private function create_table_footer() {

        return '<tr class="spacer">
                    <th scope="row"></th>
                    <td></td>
                </tr>
            </table>';

    }

    private function create_table_row( $label = '', $help = '', $el, $aPh ) {

        $str = '<tr valign="top">
                     <th scope="row">

                        <label for="">' . $label. '</label>
                        <br><span class="aPh">' . $aPh. '</span>
                        </th>';

        $str .= '<td>' . $el . '<br><span class="help"><p class="description">' . $help . '</p></span></td>';

        $str .= '</tr>';

        return $str;

    }

}