<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/admin
 * @author     Dachcom Digital <shagspiel@dachcom.com>
 */
class Dachcom_Form_Generator_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * This ist required to register Plugin in Dachcom Repo
	 *
	 * @var DachcomPluginObserver_Connector
	 */
	private $dpo_connector;

	/**
	 * @var DachcomFormGenerator_Settings
	 */
	private $settings = NULL;

	/**
	 * if user is Dachcom, this is true
	 * @var bool
	 */
	private $is_support_team = FALSE;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->settings = new DachcomFormGenerator_Settings();

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/dachcom-form-generator-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/dachcom-form-generator-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function register_plugin() {

		$this->dpo_connector = new DachcomPluginObserver_Connector();
		$this->dpo_connector->connect_to_plugin_checker( $this->plugin_name, plugin_dir_path( dirname( __FILE__ ) ) . $this->plugin_name . '.php' );

	}

	/**
	 * Add your Settings here, if want to extend the Dachcom Plugins
	 */
	public function plugin_core_settings_api_init() {

	}

	function setup_backend_menu(){

		$user = wp_get_current_user();

		if($user->ID == 0)
			return false;

		$user_login_name = $user->data->user_login;
		$this->is_support_team = $user_login_name == 'dachcom' ? TRUE : FALSE;

		if( $this->is_support_team )
			$this->load_backend_menu();

	}

	function load_backend_menu() {

		$page_slug = 'dachcom-form-layouts';

		if (isset($_REQUEST['action']) && 'store-dfg-values' == $_REQUEST['action'] ) {

			$this->update_values();

		}

		if (isset($_REQUEST['saved']) && $_REQUEST['saved']) {

			echo '<div id="message" class="updated faded"><p>Form-Einstellungen wurden gespeichert</p></div>';

		}

		add_menu_page(

			'Dachcom Form Generator',
			__('Dachcom Forms', $this->plugin_name),
			'manage_options',
			$page_slug,
			array($this, 'create_form_interface')

		);

		$form_elements = $this->settings->get_config_form();

        if( empty( $form_elements ) )
            return false;

		foreach( $form_elements as $formID => $form ) {

			$menu_title = $formID;
			$menu_title = ucfirst( str_replace( array('form_', '_'), array('', ' '), $menu_title ) );

			add_submenu_page(

				$page_slug,
				'Einstellungen: '. $formID,
				$menu_title,
				'manage_options',
				$formID,
				array($this, 'create_sub_form_interface')

			);

		}

		add_submenu_page(

			$page_slug,
			'Einstellungen: Allgemeines' ,
			'Allgemeines' ,
			'manage_options' ,
			'dfg-default_settings' ,
			array($this, 'create_default_form_data_interface')

		);

		//there we remove the first submenu-page (this one does apply wordpress by default )
		remove_submenu_page( $page_slug, $page_slug);

	}

	function update_values() {

		global  $wpdb;

		$formID = $_POST['dfg-form-id'];

		$ignore_vals = array( 'save', 'action', 'dfg-form-id');

		foreach($_POST as $group_name => $values) {

			if( !is_array( $values  ) )
				continue;

			foreach( $values as $group_value_name => $group_value ) {

				if(empty( $group_value )) {

					$wpdb->query( 'DELETE FROM ' . $wpdb->prefix . 'dcd_form_generator WHERE form_key = "' . $group_value_name . '"' );
					continue;

				}

				if( in_array($group_value_name, $ignore_vals) )
					continue;

				$row_count = $wpdb->get_var('SELECT COUNT(*) FROM ' . $wpdb->prefix . 'dcd_form_generator WHERE formID = "'.$formID.'" AND form_key = "'. $group_value_name . '"');

				//insert
				if( $row_count == 0 ) {

					$wpdb->insert( $wpdb->prefix."dcd_form_generator" , array('formID' => $formID, 'form_key' => $group_value_name, 'form_value' => $group_value, 'form_type' => $group_name) );

				} else {

					//update
					$wpdb->update( $wpdb->prefix."dcd_form_generator" , array('form_value' => $group_value), array('formID' => $formID, 'form_key' => $group_value_name ) );
				}

			}

		}

		$query = str_replace('&saved=true', '', $_SERVER['QUERY_STRING'] );

		header("Location: admin.php?" .$query . '&saved=true' );
		exit;

	}

	function create_form_interface(){

		$this->render_header();

		$this->render_welcome_content();

		$this->render_footer();

	}

	function create_sub_form_interface() {

		$this->render_header();

		$this->render_form_to_edit();

		$this->render_footer();

	}

	function render_form_to_edit( ) {

		$formID = $_GET['page'];

		$form_elements = $this->settings->get_config_form();
		$displayFormName = ucfirst( str_replace( array('form_', '_'), array('', ' '), $formID ) );

		if( !isset($form_elements[ $formID ]['configurations']) || !isset($form_elements[ $formID ]['fields'])  ) {

			echo '<p>Keine config und/oder keine Felder vorhanden ...</p>';
			return;

		}

		$configurationFields = $form_elements[ $formID ]['configurations'];
		$formFields = $form_elements[ $formID ]['fields'];

		$this->render_form_header();

		$str = '<h3>Einstellungen: '.$displayFormName.'</h3>';

		//render config options for current form
		if( !empty( $configurationFields ) ) {

			$str .= $this->render_form_configuration_fields( $formID, $configurationFields );

		}

		if( !isset( $configurationFields['deactivate_mail_send'] ) || $configurationFields['deactivate_mail_send'] == FALSE ) {

			//render the actual fields for the current form
			if( !empty( $formFields ) ) {

					$str .= $this->render_form_element_fields( $formID, $formFields );

			}

		}

		echo $str;

		$this->render_form_footer();

	}

	function create_default_form_data_interface(){

		$this->render_header();
		$this->render_form_header();

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfg-admin-form-general-settings.php';

		$this->render_form_footer();
		$this->render_footer();

	}


	/**
	 * Renders the Configuration Part of the Form
	 */
	function render_form_configuration_fields( $formID, $setted_config = array() ) {

		$fieldConfigParser = new FormField_Config();

		$fieldConfigParser->set_admin( $this->is_support_team == TRUE );
		$fieldConfigParser->set_default_config_data( $this->settings->get_form_configuration_data() );
		$fieldConfigParser->set_data( $setted_config );
		$fieldConfigParser->set_db_data( $this->get_form_vals( $formID ) );

		return $fieldConfigParser->parse();

		//require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfg-admin-form-config.php';

	}

	/**
	 * Renders the Elements Part of the Form
	 */
	function render_form_element_fields( $formID, $setted_fields = array() ){

		$fieldElementParser = new FormField_Element();

		$fieldElementParser->set_data( $setted_fields );
		$fieldElementParser->set_db_data( $this->get_form_vals($formID) );

		return $fieldElementParser->parse();

	}

	function render_form_header( ){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfg-admin-formheader.php';
	}

	function render_form_footer( ){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfg-admin-formfooter.php';
	}

	function render_header(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfg-admin-default-header.php';
	}

	function render_footer(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfg-admin-default-footer.php';
	}

	function render_welcome_content(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dfg-admin-default-welcome.php';
	}


	function get_form_vals( $formID ) {

		global $wpdb;

		$tableName = $wpdb->prefix;

		$results = $wpdb->get_results("SELECT form_key, form_value FROM ". $tableName ."dcd_form_generator WHERE formID = '" . $formID . "'", OBJECT_K);

		return $results;

	}

}
