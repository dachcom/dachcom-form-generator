var dcd_form_field_helper;
jQuery(document).ready(function( $ ) { dcd_form_field_helper = new dachcom_fgh( $ ); });

var dachcom_fgh = function($) {

	/* main handler */
	var constructor = {
			
		version : '1.7.1',
		
		init : function() {

			var root = this;

			// START
			$(document).on('click', '.dachcom-form-generator-submit-ajax', function(ev){

				var form = $(this).closest('form');
	    		
	    		var hasResponse = $(form).find('.respond').length > 0;
	    		
		    	var messageBox = $(form).find('.respond'),
					spinner = $(form).find('.logger'),
					sendButton = $(form).find('input[type="submit"]');
		    	
		    	var __success = function( response ) {
		    		
		    		var canResetForm = true;
		    		
					if(response.status == 'ok') {
                		
						$(form).trigger('dachcom-form-generator/success', response);
						
					} else {
						
						$(form).trigger('dachcom-form-generator/error', response);
						canResetForm = false;
					}
					
		    		sendButton.removeAttr("disabled");

	    	 		if( hasResponse ) {
	    	 			
	    	 			$(spinner).fadeOut('slow', function() {
	    	 				
	    	 				spinner.removeClass('form-preloader').hide();

	    	 				$(messageBox).addClass('success').show().html( response.message );
	    	 				
	    	 			});
	    	 				
	    	 		}
	    	 		
	    	 		if( !dcd_form_field_helper.isArea51() && canResetForm == true )
	    	 			validate.resetForm();
	    	 		
		    	};
		    
		    	var validate = $(form).validate({
		    		
					submitHandler: function(form) {
						
						$(form).ajaxSubmit( {
							
							dataType:  'json', 
				    	 	'data' : { 
				    	 		'isAjax' : true
				    	 	},
				    	 	beforeSubmit: function(arr, $form, options) { 

				    	 		$(form).trigger('dachcom-form-generator/beforeSubmit');
				    	 		
				    	 		if( hasResponse ) {
				    	 			
				    	 			spinner.addClass('form-preloader').show();
					    	 		$(messageBox).removeClass('success').html('').hide();
					    	 		
				    	 		}
				    	 		
				    	 		if( !dcd_form_field_helper.isArea51() )
			                		sendButton.attr("disabled", "disabled");
			                	
				    	 	},
				    	 	
				    	 	success: function(resp) {

				    	 		__success( resp );
				    	         
				    	    },
				    	    error : function(resp) {
				    	    	
				    	    	$(form).trigger('dachcom-form-generator/error', resp);
				    	    	
				    	    }
				    	 	
					     }); 
						
					}
					 
				});
				
		    	//add reset handler
				$('.resetForm').click(function() { validate.resetForm(); });
				
	    	});
			
			var forms = $(document).find('form.dachcom-form-generator-ajax');

	    	if(forms.length <= 0  )
	    		return;

	    	forms.each(function(i, form) {
	    		//setup toggle events
	    		root.setupToggler( $(form) );
	    		//setup special rules (like dependencies )
	    		root.addCustomRules( $(form ) );
	    		//setup repeater fields, if available
	    		root.setupRepeaterFields( $(form) );
	    	});

		},
		
		isArea51 : function() {
			
			return window.location.hostname.indexOf('.dev') != -1;
			
		},
		
		setupToggler : function( form ) {
		
			var validInputs = 'input:not(:radio):not(:checkbox),textarea,select';
			var valCheckInputs = 'input[type="radio"],input[type="checkbox"]';
			var valCheckInputsChecked = 'input[type="radio"]:checked,input[type="checkbox"]:checked';

			var toggleGroupData = function( el, force_disable ) {
				
				if( el.hasClass('enabled' ) ) {
					
					el.find( validInputs ).val('').attr('disabled', true);
					el.find( valCheckInputsChecked ).trigger('click');
					el.find( valCheckInputs ).attr('disabled', true);
					
					el.removeClass('enabled').addClass('disabled');
					
				} else if( force_disable && el.hasClass('disabled' ) ) {
					
					el.find( validInputs ).val('');
					el.find( valCheckInputsChecked ).trigger('click');
	
					//dont change any class, because user wants to force disabling, so leave it disabled....
					
				} else if( !force_disable && el.hasClass('disabled' ) ) {
					
					el.find( validInputs ).attr('disabled', false);
					el.find( valCheckInputs ).attr('disabled', false);
						
					el.removeClass('disabled').addClass('enabled');
	
				} else if( el.hasClass('visible' ) ) {
					
					el.find( validInputs ).val('');
					el.find( valCheckInputsChecked ).trigger('click');
					
					el.removeClass('visible').addClass('invisible');
					
				} else if( !force_disable && el.hasClass('invisible' ) ) {
						
					el.removeClass('invisible').addClass('visible');
					
				}
				
			};
			
			form.find('.group.disabled').each(function(i,g) {

                $(g).find( validInputs ).prop('disabled', true);

			});

			form.find('[data-toggle-target]').on('change', function( ev ) {
				
				var target = $(this).data('toggle-target');
				
				if( $(this).is( 'select' ) ) {
					
					//first, disabled
					$(this).find('option:not(:selected)').each(function() {
					   
					    if( $('#' + this.value ).length != 0)
					    	toggleGroupData( $('#' + this.value ), true );
					});
				
					//activate selected
					 if( $( '#' + $(this).find(':selected').val()  + '.group' ).length != 0)
						toggleGroupData( $( '#' + $(this).find(':selected').val()  + '.group' ) );
					 
				//same here, we need to disable first the other ones.
				} else if( $(this).is(':radio') ) {
					
					form.find('input[name="' + $(this).attr('name')  + '"]').not(this).each(function() {
						
					   if( $('#' + this.value ).length != 0) {
						   toggleGroupData( $('#' + this.value ), true );
					   }
						
					});
					
					if( $( '#' + target  + '.group' ).length != 0)
							toggleGroupData( $( '#' + target + '.group' ) );
					
				//its a checkbox.
				} else {

					if( $('#' + target ).length != 0)
						toggleGroupData( $('#' + target ) );
					
				}
				
			});
			
			
		},
		
		addCustomRules : function( form ) {
			
			//dependencie rule
			//dependencies-rule-name="delivery" data-dependencies-target="delivery-collapse" data-dependencies-reverse="true"
			form.find('input[data-dependencies]').each(function( i, el ) {
				
				var element = $(el),
					depName = element.data('dependencies'), 
					depTarget = $('#' +  element.data('dependencies-target') );
				
				jQuery.validator.addMethod(depName, function(value, element) {
					
					if ( $(element).is(":checked") )
						return depTarget.length;
					
					return  !this.optional(element);
					
				}, "");
				
			});
			
		},
		
		setupRepeaterFields : function( form ) {
				
			form.find('a.repeat-button').click( function( ev ) {
				
				ev.preventDefault();
				
				var b = this;
				
				var repeaterID = $(b).data('repeater-id'),
					template = $(b).next('.clone'),
					cloneRange = $(b).parents('.form'),
					maxItems = $(b).data('max-fields');

				//limit reached
				if( maxItems > 0 && maxItems == $(b).data('counter-max-fields') ) {
					
					return;
					
				}
				
				var counter = cloneRange.find('.repeater-' + repeaterID).length + 1;

				$(b).data('counter-max-fields', counter );

				//clone data
				var newData = template.clone();
				
				//now format newData Row
				newData.removeClass('clone').addClass('repeater-' + repeaterID);
				
				if( counter > 1)
					newData.css('borderTop', 0);
				
				//setup counter
				var blockTitle = newData.find('.block-title'),
					newBlockTitle = blockTitle.text().replace('%indicator', counter + 1);
				
				blockTitle.text( newBlockTitle );
				
				//setup delete
				var removeBlock = newData.find('.remove-block');
				
				removeBlock.click( function(ev) { 
					
					ev.preventDefault(); 
					
					$(b).show();
					$(b).data('counter-max-fields', counter - 1 );
					
					if( maxItems > 0 ) 
						$(b).find('.view-repeater-counter').text('(' + ( maxItems - ( counter - 1) ) + ')');
					
					$(this).parents('.repeater-data').remove(); 
					var last = $(b).prev('.repeater-data');
					
					if( last && last.find('.remove-block') )
						last.find('.remove-block').show();
					
				});
				
				//now add real data attribute
				newData.find('*[data-name]').each( function (i, el) {
					
					var dataName = $(el).attr('data-name'),
						newName = 'repeater_field_' + ( counter+1 ) + '_' + dataName;
					
					if( dataName != 'repeater_field') {
						$(el).attr( 'name', newName);
					} else {
						$(el).attr( 'name', 'repeater_field_' + ( counter+1 ));
						$(el).val( newBlockTitle );
					}
					
				});
				
				//now hide all other block removables
				cloneRange.find('.repeater-data[class!="repeater-' + repeaterID +'"]').each( function (i, el) {
					
					if( !$(el).hasClass('clone'))
						$(el).find('.remove-block').hide();
					
				});
				
				//now inject
				$(b).before( newData ).promise().done(function() {
					
					$(form).trigger('dachcom-form-generator/repeater-added', newData);
					
				});
				
				if( maxItems > 0 ) 
					$(b).find('.view-repeater-counter').text('(' + ( maxItems - counter ) + ')');

				//limit reached
				if( maxItems > 0 && maxItems == $(b).data('counter-max-fields') ) {
					
					$(b).hide();
					
				}
				
			});
			
		}

	};

	constructor.init();
	
	return constructor;
	
};