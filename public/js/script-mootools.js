var mrs_form_field_helper;

window.addEvent('domready', function(){ mrs_form_field_helper = new mrs_ffh(  ); });

var mrs_ffh = new Class({
	
	Implements: [Options, Events],
	
	options: { },
     
	initialize: function(targets,options) {
			
		Locale.use('de-DE');
		
		var root = this;
		
		// START
		
		var __success = function( response, form, spinner, messageBox, sendButton, hasResponse, validator ) {
    		
    		var canResetForm = true;
    		
			if(response.status == 'ok') {
        		
				form.fireEvent('mrs-form-field/success', response);
				
			} else {
				
				form.fireEvent('mrs-form-field/error', response);
				canResetForm = false;
			}
			
    		sendButton.removeAttribute("disabled");

	 		if( hasResponse ) {
	 			
	 			spinner.hide();
	 				
 				spinner.removeClass('form-preloader').hide();
 				messageBox.addClass('success').show().set( 'html', response.thank_you );
 			
	 				
	 		}
	 		
	 		if( !root.isArea51() && canResetForm == true ) {
	 			validator.reset(); 
				form.reset(); 
	 		}
	 		
		};
		
		var __onFormSubmit = function( isValid, form, ev, spinner, messageBox, sendButton, hasResponse, validator) {
			
			var postData = form.toQueryString().parseQueryString();
			 
	    	if( postData.ref )
	    		delete postData.ref;
			 
	    	postData.isAjax = true;
	    	
			 if( !isValid ) 
				 return false;
			 
			 new Request.JSON({
				 
				url: form.getAttribute('action'),
	    	 	data : postData,
	    	 	onRequest: function( ev ) { 
	    	 		
	    	 		form.fireEvent('mrs-form-field/beforeSubmit');
	    	 		
	    	 		if( hasResponse ) {
	    	 			
	    	 			spinner.addClass('form-preloader').show();
	    	 			messageBox.removeClass('success').set('html', '').hide();
		    	 		
	    	 		}
	    	 		
	    	 		if( !root.isArea51() )
	    	 			sendButton.setAttribute("disabled", "disabled");
                	
	    	 	},
	    	 	
	    	 	onSuccess: function(resp) {
	    	 		
	    	 		__success( resp, form, spinner, messageBox, sendButton, hasResponse, validator );
	    	         
	    	    },
	    	    onError : function(resp) {
	    	    	
	    	    	form.fireEvent('mrs-form-field/error', resp);
	    	    	
	    	    }
	    	 	
	    	}).send();
				
			 
		};
		
		
		var forms = $(document.body).getElements('form.mrs-form-field-ajax');
    	
    	if(forms.length <= 0  )
    		return;
    	
    	forms.each(function(form, i) {
    		
    		var hasResponse = form.getElement('.respond');
    		
	    	var messageBox = form.getElement('.respond'),
				spinner = form.getElement('.logger'),
				sendButton = form.getElement('input[type="submit"]');
	    	
    		//setup toggle events
    		root.setupToggler( form );
    		//setup special rules (like dependencies )
    		root.addCustomRules( form );
    		//setup repeater fields, if available
    		root.setupRepeaterFields( form );
    		
    		var validator = new Form.Validator.Inline(form, {
	    		
	    	    onFormValidate: function(isValid, form, ev) {
	    	    	__onFormSubmit( isValid, form, ev, spinner, messageBox, sendButton, hasResponse, validator);
	    	    	
	    		 }
	    		
	    	});
    	
    		form.getElement('.resetForm').addEvent('click', function() { 
    			
				console.log('reset');
				validator.reset(); 
				form.reset(); 
				
			});
    		
    		form.addEvent('submit', function(ev) {
    			
    			ev.preventDefault();
    			validator.validate();
    			
    		});
    		
    	});
	
	},
	
	isArea51 : function() {
		
		return window.location.hostname.indexOf('dev.mrs.to') != -1;
		
	},
	
	setupToggler : function( form ) {
	
		var validInputs = 'input[type="text"],input[type="int"],input[type="date"],textarea,select';
		var valCheckInputs = 'input[type="radio"],input[type="checkbox"]';
		
		var toggleGroupData = function( el, force_disable ) {
			
			
			if( el.hasClass('enabled' ) ) {
				
				el.getElements( validInputs ).set('value', '').setAttribute('disabled', true);
				el.getElements( valCheckInputs ).set('checked', false);
				
				el.removeClass('enabled').addClass('disabled');
				
			} else if( !force_disable && el.hasClass('disabled' ) ) {
				
				el.getElements( validInputs ).set('disabled', false);
				el.getElements( valCheckInputs ).set('checked', false);
				el.removeClass('disabled').addClass('enabled');
				
			} else if( el.hasClass('visible' ) ) {
				
				el.getElements( validInputs ).set('value', '');
				el.getElements( valCheckInputs ).set('checked', false);
				
				el.removeClass('visible').addClass('invisible');
				
			} else if( !force_disable &&  el.hasClass('invisible' ) ) {
					
				el.removeClass('invisible').addClass('visible');
				
			}
			
		};
		
		form.getElements('.group.disabled').each(function(g,i) {
			g.getElements( validInputs ).setAttribute('disabled', true);
			
		});
		
		form.getElements('[data-toggle-target]').addEvent('change', function( ev ) {
			
			var el = this;
			
			var target = el.getAttribute('data-toggle-target');
			
			if( el.get('type') == 'select-one' ) {
				
				//first, disabled
				el.getElements('option:not(:selected)').each(function(e) {
					
				    if( $( e.get('value') ) )
				    	toggleGroupData( $(  e.get('value') ), true );
				});
			
				//activate selected
				 if( form.getElement( '#' + el.getSelected().get('value')  + '.group' ) )
					toggleGroupData( form.getElement( '#' + el.getSelected().get('value') + '.group' ) );
				 
			} else {
				
				if( $( target ).length != 0)
					toggleGroupData( $( target ) );
				
			}
				
		});
		
	},
	
	addCustomRules : function( form ) {
		
		//dependencie rule
		//dependencies-rule-name="delivery" data-dependencies-target="delivery-collapse" data-dependencies-reverse="true"
		form.getElements('input[data-dependencies]').each(function( i, el ) {
			
			var element = $(el),
				depName = element.data('dependencies'), 
				depTarget = $('#' +  element.data('dependencies-target') );
			
			jQuery.validator.addMethod(depName, function(value, element) {
				
				if ( $(element).is(":checked") )
					return depTarget.length;
				
				return  !this.optional(element);
				
			}, "");
			
		});
		
	},
	
	setupRepeaterFields : function( form ) {
			
		form.getElements('a.repeat-button').addEvent('click', function( ev ) {
			
			ev.preventDefault();
			
			var b = this;
			
			var repeaterID = $(b).attr('data-repeater-id'),
				template = $(b).next('.clone'),
				cloneRange = $(b).parents('.blockItem');
			
			var counter = cloneRange.getElements('.repeater-' + repeaterID).length;
			
			if( counter == 0) 
				counter = 1;
			else
				counter++;
			
			//clone data
			var newData = template.clone();
			
			//now format newData Row
			newData.removeClass('clone').addClass('repeater-' + repeaterID);
			if( counter > 1)
				newData.css('borderTop', 0);
			
			//setup counter
			var blockTitle = newData.getElements('.block-title'),
				newBlockTitle = blockTitle.text().replace('%indicator', counter + 1);
			
			blockTitle.text( newBlockTitle );
			
			//setup delete
			var removeBlock = newData.getElements('.remove-block');
			removeBlock.click( function(ev) { 
				ev.preventDefault(); 
				
				$(this).parents('.repeater-data').remove(); 
				var last = $(b).prev('.repeater-data');
				
				if( last && last.getElements('.remove-block') )
					last.getElements('.remove-block').show();
				
			});
			
			//now add real data attribute
			newData.getElements('*[data-name]').each( function (i, el) {
				
				var dataName = $(el).attr('data-name');
					newName = 'repeater_field_' + ( counter+1 ) + '_' + dataName;
				
				if( dataName != 'repeater_field') {
					$(el).attr( 'name', newName);
				} else {
					$(el).attr( 'name', 'repeater_field_' + ( counter+1 ));
					$(el).val( newBlockTitle );
				}
				
			});
			
			//now hide all other block removables
			cloneRange.getElements('.repeater-data[class!="repeater-' + repeaterID +'"]').each( function (i, el) {
				
				if( !$(el).hasClass('clone'))
					$(el).getElements('.remove-block').hide();
				
				
			});
			
			//now inject
			$(b).before( newData ).promise().done(function() {
				
				//$(newData).prev('div').effect("highlight", {}, 400);
				
				$(form).trigger('mrs-form-field/repeater-added', newData);
				
			});
			
		});
		
	}

});