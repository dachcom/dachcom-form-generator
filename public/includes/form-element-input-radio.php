<?php

class FormElement_Input_Radio extends FormElement {

    var $label_wrap =   '<label class="%s">%s</label>';
    var $label_class = '';

    function __construct( $el_name, $el_params, $form_type ) {

        parent::__construct( $el_name, $el_params, $form_type );

    }

    function render_element(  ) {

        $this->add_class('radio');

        $value = (array) $this->el_params['value'];

        $html = '';

        $real_name = $this->get_attribute('name');

        //we could have multiple elements - create an group data attribute
        $unique_group_id = uniqid('cd-');

        foreach( $value as $c => $val ) {

            parent::change_value( $val['value'] );
            parent::change_attribute('id', $real_name . '-' . $c );
            parent::change_attribute('name', $real_name . ( count( $value ) == 1 ? '' : '[]' ) );
            parent::change_attribute('data-group-id', $unique_group_id);

            if( isset( $val['class']) && !empty( $val['class'] ) )
                parent::add_class( $val['class'] );

            $str = parent::render_element();

            $html .= '<div class="radio">';

            $html .= sprintf(

                $this->label_wrap,
                $this->label_class,
                $str . "\n" . $val['label'] . $this->_getDescription( $val )

            );

            $html .= '</div>';

        }

        return $html;

    }

    private function _getDescription( $val ) {

        if( isset( $val['desc']) && !empty( $val['desc'] ) )
            return sprintf( $this->description_wrap, $val['desc']);

        return '';

    }

}