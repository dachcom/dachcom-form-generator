<?php

class FormElement_Textarea extends FormElement {

    var $close_bracket = '></textarea>';

    function __construct( $el_name, $el_params, $form_type ) {

        $this->extend_valid_attributes( array( 'placeholder' ) );

        parent::__construct( $el_name, $el_params, $form_type );

        $this->add_class('form-control');

    }

}