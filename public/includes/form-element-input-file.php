<?php

class FormElement_Input_File extends FormElement {

    function __construct( $el_name, $el_params, $form_type ) {

        $this->extend_valid_attributes( array( 'accept', 'multiple' ) );

        parent::__construct( $el_name, $el_params, $form_type );

        $accept = isset($el_params['accept']) && is_array($el_params['accept']) ? implode(",", $el_params['accept']) : '';

        if( !empty( $accept ) )
            $this->add_attribute('accept', $accept );

        $this->add_class('form-control');

    }

}