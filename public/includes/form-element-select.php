<?php

class FormElement_Select extends FormElement {


    var $close_bracket = '>';

    function __construct( $el_name, $el_params, $form_type ) {

        $this->extend_valid_attributes( array( 'multiple' ) );

        parent::__construct( $el_name, $el_params, $form_type );

        $this->add_class('form-control');

    }

    function render_element(  ) {

        $html = parent::render_element();

        $value = (array) $this->el_params['value'];

        foreach( $value as $group_val => $val ) {

            if( is_array( $val ) ) {

                $html .= $this->render_optgroup_open( $group_val );

                foreach( $val as $v => $k )
                    $html .= $this->render_option($v, $k);

                $html .= $this->render_optgroup_close();

            } else {

                $html .= $this->render_option($group_val, $val);

            }

        }

        $html .= '</select>';

        return $html;

    }

    function render_option( $value, $key ) {

        return '<option value="' . $value . '">' . $key . '</option>';

    }

    function render_optgroup_open( $groupval = "" ) {

        return '<optgroup label="' . $groupval . '">';

    }

    function render_optgroup_close() {

        return '</optgroup>';

    }


}