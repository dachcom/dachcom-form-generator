<?php

class FormElement_Input_Hidden extends FormElement {

    var $wrap = '%s';

    function __construct( $el_name, $el_params, $form_type ) {

        //do not use an title wrapper
        unset( $el_params['title']);

        parent::__construct( $el_name, $el_params, $form_type );

        $this->add_class('form-control');

    }

}