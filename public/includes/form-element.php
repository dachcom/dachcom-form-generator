<?php

class FormElement {

    /**
     * Global Info of Form Rendering (inline or default)
     * @var string
     */
    var $form_type = '';

    var $el_params = array();

    var $el_name = '';

    var $el_value = '';

    var $wrap =                     '<div class="form-group">%s</div>';
    var $input_wrap =               '<div class="%s">%s</div>';
    var $input_group_wrap =               '<div class="input-group">%s</div>';
    var $description_wrap =         '<span class="description">%s</span>';

    var $main_label_wrapper =       '<label for="%s" class="%s">%s</label>';
    var $main_label_class =         'control-label';

    private $valid_element_types = array();
    private $valid_attributes = array( 'id', 'name', 'disabled', 'required', 'title', 'data-');

    /**
     * all valid attributes, setted in config
     * @var array
     */
    protected $attributes = array();

    /**
     * a copy of $attributes
     * so we can reset them easily
     * @var array
     */
    private $_attributes = array();

    var $is_required = FALSE;
    var $has_multiple_values = FALSE;

    var $required_symbol = '';
    var $required_symbol_wrapper = '<span class="required">%s</span>';

    var $tag_type = '';
    var $sub_tag_type = '';

    var $open_bracket = '<';
    var $open_tag = '';

    var $close_bracket = ' />';

    var $input_wrap_class = '';
    var $input_classes = array();

    /**
     * a copy of $input_classes
     * so we can reset them easily
     * @var array
     */
    private $_input_classes = array();


    function __construct( $el_name, $el_params, $form_type = '' ) {

        $this->form_type = $form_type;

        $this->el_name = $el_name;
        $this->el_params = $el_params;

        $this->valid_element_types = array(

            'input' =>

                array('text', 'file', 'hidden', 'email', 'number', 'password', 'checkbox', 'radio'),

            'select' => '',
            'file' => '',
            'textarea' => ''

        );

        $first_tag = $this->el_params ['type'];
        $second_tag = isset( $this->el_params ['sub_type'] ) ? $this->el_params ['sub_type'] : '';

        if( isset( $this->valid_element_types[ $first_tag ]) )
            $this->tag_type = $first_tag;

        if( !empty( $second_tag ) && in_array( $second_tag, $this->valid_element_types[ $first_tag ]) )
            $this->sub_tag_type = $second_tag;

        $this->setup_required_field();
        $this->add_attributes();
        $this->check_dependencies();

        if( !empty( $this->el_params['class'] ) )
            $this->add_classes( (array) $this->el_params['class'] );

    }

    function get_form_layout() {

        return sprintf( $this->wrap, $this->render_content() ) . "\n";

    }

    function render_content() {

        $label = $this->render_title();

        $element = $this->render_element();

        return $label . $element;

    }

    function render_element() {

        $str = '';

        $str .= $this->render_addon('prefix');

        $str .= $this->open_bracket . $this->tag_type . ' ';

        $str .= $this->render_class();
        $str .= $this->render_type();
        $str .= $this->render_value();
        $str .= $this->render_attributes();

        $str .= $this->close_bracket;

        $str .= $this->render_addon('suffix');

        //if input has group addons, wrap it in to input group wrap
        if( $this->has_param('prefix') || $this->has_param('suffix') ) {
            $str = sprintf( $this->input_group_wrap, $str);
        }


        $this->_reset_attributes();
        $this->_reset_class();

        return $str;

    }

    function render_type( ) {

        if( !empty( $this->sub_tag_type ) )
            return ' type="' . $this->sub_tag_type . '"';

    }

    function render_class() {

        $classes = '';

        foreach($this->input_classes as $cl ) {

            $classes .= $cl . ' ';
        }

        $classes = trim( $classes );

        return 'class="' .$classes . '"';

    }

    function render_value( ) {

        return ' value="' . $this->el_value . '"';

    }

    function render_addon( $type ) {

        if( ( $type == 'prefix' && !isset( $this->el_params['prefix'] ) ) ||
            ( $type == 'suffix' && !isset( $this->el_params['suffix'] ) )
        )
            return;

        return '<span class="input-group-addon">' . $this->el_params[ $type ] . '</span>';

    }

    function change_value( $new_value ) {

        $this->el_value = $new_value;

    }

    function render_title() {

        if(  !isset( $this->el_params['title'] ) || empty( $this->el_params['title'] ) )
            return '';

        $str = '';

        $str .= $this->el_params['title'];

        if( $this->is_required && !empty( $this->required_symbol ) )
            $str .= sprintf( $this->required_symbol_wrapper, $this->required_symbol );

        $label_class = $this->main_label_class;

        if( $this->form_type == 'inline' ) {

            $label_class .= ' sr-only';

        }

        return sprintf( $this->main_label_wrapper, $this->el_params['id'], $label_class, $str );

    }

    function render_attributes() {

        $str = '';

        foreach( $this->attributes as $attr_name => $attr_value ) {

            if( $attr_name == 'name' && $this->get_attribute( 'multiple' ) !== FALSE)
                $attr_value = $attr_value . '[]';

            $str .= $attr_name . '=' . '"' . $attr_value . '" ';

        }

        return $str;

    }

    function add_attributes() {

        foreach( $this->el_params as $attr_name => $attr_value ) {

            if( $this->is_valid_attributes( $attr_name ) ) {

                $this->add_attribute( $attr_name, $attr_value );
            }

        }

        //add name manually, because its not given in the config array
        $this->add_attribute( 'name', $this->el_name );

        //backup attributes
        $this->_attributes = $this->attributes;

    }

    function add_attribute( $name, $value ) {

        $this->attributes[ $name ] = $value;

    }

    function change_attribute( $attr_name, $new_value ) {

        $this->attributes[ $attr_name ] = $new_value;

    }

    function has_attribute( $attr_name ) {

        if( isset( $this->attributes[ $attr_name ] ) )
            return true;

        return false;

    }

    function get_attribute( $attr_name ) {

        if( isset( $this->attributes[ $attr_name ] ) )
            return $this->attributes[ $attr_name ];

        return false;

    }

    function is_valid_attributes( $attr ) {

        if( in_array( $attr, $this->valid_attributes ) || substr( $attr, 0, 5 ) == 'data-' )
            return true;

    }

    function add_classes( $classes = array() ) {

        foreach( $classes as $c ) {

            $this->add_class( $c );

        }

        //backup classes
        $this->_input_classes = $this->input_classes;

    }

    function add_class( $class_name ) {

        $this->input_classes[ ] = $class_name;

        $this->input_classes = array_unique( $this->input_classes );

    }

    function has_param( $param_name ) {

        if( isset( $this->el_params[ $param_name ] ) )
            return true;

        return false;

    }

    function check_dependencies() {

        //be sure, a placehoder is setted if we are in inline mode.
        if( !$this->has_attribute( 'placeholder' ) && $this->form_type === 'inline' && $this->is_valid_attributes('placeholder') )
            $this->add_attribute('placeholder', $this->get_attribute( 'title' ) );

    }

    function set_required_symbol( $required_symbol ) {

        $this->required_symbol = $required_symbol;

    }

    function set_required_symbol_wrapper( $required_symbol_wrapper ) {

        $this->required_symbol_wrapper = $required_symbol_wrapper;

    }

    function setup_required_field() {

        if( !isset( $this->el_params['required'])  )
            return false;

        $required_data = $this->el_params['required'];

        if( isset( $this->el_params['required'] ) )
            unset( $this->el_params['required'] );

        if( $required_data === false || $required_data === 'false' ) {

            return false;

        }

        $this->is_required = true;

        $this->add_attribute('required', 'true');
        $this->add_attribute('data-rule-required', 'true');

        if( !is_array( $required_data ) ) {

            $this->add_attribute('data-validators', 'required');

            return;

        }

        foreach( $required_data as $rule ) {

            $this->add_attribute( 'data-rule-' . $rule['rule'], 'true');

            if( isset( $rule['msg']) && !empty( $rule['msg'] ) )
                $this->add_attribute( 'data-msg-' . $rule['rule'], $rule['msg'] );
            elseif( isset( $rule['msg']) && $rule['msg'] === FALSE )
                $this->add_attribute( 'data-msg-' . $rule['rule'],"" );
        }

    }


    private function _reset_attributes() {

        $this->attributes = $this->_attributes;
    }

    private function _reset_class() {

        $this->input_classes = $this->_input_classes;
    }

    public function extend_valid_attributes( $attributes = array() )
    {

        $this->valid_attributes = array_merge( $attributes, $this->valid_attributes);

    }

}