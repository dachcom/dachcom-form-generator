<?php

class FormElement_Input_Number extends FormElement {


    function __construct( $el_name, $el_params, $form_type ) {

        $this->extend_valid_attributes( array( 'step', 'size', 'pattern', 'max', 'min', 'inputmode', 'maxlength', 'placeholder' ) );

        parent::__construct( $el_name, $el_params, $form_type );

        $this->add_class('form-control');

    }

}