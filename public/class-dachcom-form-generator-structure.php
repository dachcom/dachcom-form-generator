<?php

class DachcomFormGenerator_Structure {

	/**
	 * a unique form-id name.
	 * @var string
	 */
	var $form_id = '';

	/**
	 * some special params.
	 * @var array
	 */
	var $params = array();

	/**
	 * all data from theme/config/form.config.php
	 * @var null
	 */
	var $form_data = NULL;

	/**
	 * all data from theme/config, merged with db
	 * @var null
	 */
	var $form_config = NULL;

	/**
	 * all data from db
	 * @var null
	 */
	var $form_generally = NULL;

	/**
	 * all overwrite data from db
	 * @var null
	 */
	var $form_db_fields = NULL;

    /**
     * Table name to store Form Data
     * @var string
     */
    var $_db_table_name = '';

    /**
     * @var DachcomFormGenerator_Settings
     */
    public $settings;

    /**
     * @var DachcomFormGenerator_ShortCodes
     */
    public $shortcodes;

	public function __construct( ) { }

	public function set_form( $form_id, $params = array() ) {

        global $wpdb;

		$this->form_id = $form_id;

		$this->params = $params;

		$this->settings = new DachcomFormGenerator_Settings();
        $this->shortcodes = new DachcomFormGenerator_ShortCodes( $this->settings );

		$config = $this->settings->get_config_form();

		$this->form_data = $this->_parse_formdata( $config[ $form_id ] );

        $this->_db_table_name =  $wpdb->prefix . 'dcd_form_generator_data';

		/**
		 * Setup Form Config Data.
		 * 1.) get config.php from theme
		 * 2) merge with DB relatives
		 */
		$this->_prepare_form_config();

		/**
		 * Setup Form Element Data.
		 * 1.) get config.php from theme
		 * 2) merge with DB relatives
		 */
		$this->_prepare_form_fields();

	}

	private function _prepare_form_fields() {

		global $wpdb;

		$fields = $wpdb->get_results( 'SELECT form_key, form_value FROM ' . $wpdb->prefix . "dcd_form_generator" . ' WHERE formID = "' . $this->form_id . '" AND form_type = "element"', OBJECT_K);

		if( !empty( $fields ) ) {

			foreach( $fields as $f_key => $f )
				$this->form_db_fields[ $f_key ] = $f->form_value;

		}

	}

	private function _prepare_form_config() {

		global $wpdb;

		$form_config_generally = $wpdb->get_results( 'SELECT form_key, form_value FROM ' . $wpdb->prefix . "dcd_form_generator" . ' WHERE form_type = "generally"', OBJECT_K);

		if( !empty( $form_config_generally ) ) {

			foreach( $form_config_generally as $g_key => $g_value ) {
				$this->form_generally[ $g_key ] = $g_value->form_value;
			}
		}

		$form_config = $wpdb->get_results( 'SELECT form_key, form_value, form_type FROM ' . $wpdb->prefix . "dcd_form_generator" . ' WHERE formID = "' . $this->form_id . '" AND form_type = "config"', OBJECT_K);

		$default_config = $this->form_data['configurations'];

		if( !empty( $form_config ) ) {

			foreach( $form_config as $c_name => $c_value ) {

				if( isset( $default_config[ $c_name ] ) )
					$default_config[ $c_name ] = $c_value->form_value;
			}
		}

		if( !empty( $default_config ) ) {
			$this->form_config = $default_config;
		}

		unset( $this->form_data['configurations'] );

	}

    function get_mail_footer_text() {

        global $dachcom_mail_shredder;

        $footerText = '';

        $footerText = $this->get_form_config('mail_signature', 'generally', $footerText);

        $footerText = html_entity_decode(stripcslashes($footerText));

        remove_filter('the_content', array($dachcom_mail_shredder, 'shredder_mail'));

        $footerText = apply_filters('the_content', $footerText);

        add_filter('the_content', array($dachcom_mail_shredder, 'shredder_mail'));

        return $footerText;

    }

    /**
     *
     * Checks the fields for special values, add a hook to each for 3th party.
     * @param $form_data
     * @return mixed
     */
    private function _parse_formdata( $form_data ) {

        foreach( $form_data['fields'] as $field_key => &$field ) {

            //apply core short codes
            $_field = $this->_parse_formfield_shortcodes( $field );

            $field = $_field;

        }

        return $form_data;

    }

    private function _parse_formfield_shortcodes( $field ) {

        if( !isset( $field['value'] ) || !is_string( $field['value'] ) )
            return $field;

        $field['value'] = $this->shortcodes->parse_shortcodes( $field['value'] );

        return $field;

    }

    /**
     * HTML Text Parser
     */

    /**
     * @param string $string
     * @param array $replacement_data
     * @return mixed|string
     */
    public function parse_default_template_placeholder( $string = '', $replacement_data = array() ) {

        preg_match_all("'%(.*?)%'si",$string, $matches);

        if(!isset($matches[1]) || empty( $matches[1] ))
            return $string;

        foreach($matches[1] as $match) {

            if(isset($replacement_data[$match]) )
                $replace = $replacement_data[$match];
            else
                $replace = '';

            $string = str_replace('%'.$match.'%', $replace, $string);

        }

        return $string;

    }

    /**
     * @param $text
     * @return mixed
     */
    public function parse_config_template_placeholder( $text ) {

        preg_match_all("'%(.*?)%'si",$text, $matches);

        if(!isset($matches[1]) || empty( $matches[1] ))
            return $text;

        foreach($matches[1] as $match) {

            $formfield = $this->get_form_element( $match );

            if( !empty( $formfield ) ) {

                $text = str_replace('%'.$match.'%', $formfield, $text);

            }

            $formconfig_field = $this->get_form_config( $match );

            if( !empty( $formconfig_field ) ) {

                $text = str_replace('%'.$match.'%', $formconfig_field, $text);

            }

        }

        return $text;

    }

	/**
	 *
	 * Get Form Config
	 *
	 * @param string $field
	 * @param string $namespace (config|generally
	 * @param string $fallback
	 * @return string
	 */
	function get_form_config( $field = '', $namespace = 'config', $fallback = '') {

		$config_array = 'form_config';

		if( $namespace == 'generally' )
			$config_array = 'form_generally';

		if( isset( $this->{$config_array}[ $field ] ) )
			return $this->{$config_array}[ $field ];
		else
			return $fallback;

	}

	function get_form_element( $field = '', $fallback = '' ) {

		if( isset( $this->form_db_fields[ $field ] ) )
			return $this->form_db_fields[ $field ];
		else
			return $fallback;

	}

}
