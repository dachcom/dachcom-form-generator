<?php

Class DachcomFormBuilder_Mailer {

    /**
     * @var DachcomFormGenerator_Structure
     */
    var $structure_class;

    var $attachment = null;
    var $attachment_type = null;

    function __construct( $structure_class ) {

        $this->structure_class = $structure_class;

    }

    /**
     * If $type is attachment, mailer assumes its an attachment data
     * If $type is server_link, mailer assumes its a link, will add it to content.
     * @param $attachment
     * @param $type
     */
    public function add_attachment( $attachment, $type ) {

        $this->attachment_type  = $type;
        $this->attachment       = $attachment;

    }

    public function has_attachment( ) {

        return !empty( $this->attachment );

    }

    /**
     * Activation Mail will be sent, if Form is a Newsletter.
     * @var $params
     * @return bool
     */
    public function send_activation_mail( $params = array() ) {

        $defaults = array(
            'activation_link' => '',
            'selected_newsletters'  => '',
            'recipient' => NULL,
        );

        $args = wp_parse_args( $params, $defaults );

        if( empty( $args['recipient'] ) || $args['recipient'] === FALSE)
            return FALSE;

        $newsletter_doi_text = $this->structure_class->get_form_config('newsletter_doi_text', 'config');
        $newsletter_doi_subject = $this->structure_class->get_form_config('newsletter_doi_subject', 'config');

        $newsletter_doi_text = str_replace( '%activation_link%', $args['activation_link'], $newsletter_doi_text );
        $newsletter_doi_text = str_replace( '%selected_newsletters%', $args['selected_newsletters'], $newsletter_doi_text );

        //remove_filter('the_content', array($newsletter_doi_text, 'shredder_mail'));

        $newsletter_doi_text = apply_filters('the_content', $newsletter_doi_text);

        //add_filter('the_content', array($newsletter_doi_text, 'shredder_mail'));

        $text = $newsletter_doi_text . $this->structure_class->get_mail_footer_text();

        return $this->prepare_mail($args['recipient'], $newsletter_doi_subject, $text, FALSE);

    }

    /**
     * Send Mail to recpient, if Newsletter subscribtion was successfully.
     * @var $params
     *  @return bool
     */
    public function send_activated_mail( $params = array() ) {

        $defaults = array(
            'selected_newsletters'  => '',
            'recipient' => NULL,
        );

        $args = wp_parse_args( $params, $defaults );

        $success_subject = $this->structure_class->get_form_config( 'newsletter_success_subject', 'config' );
        $success_text = $this->structure_class->get_form_config( 'newsletter_success_text', 'config' );

        $success_text = str_replace( '%selected_newsletters%', $args['selected_newsletters'], $success_text );

        $this->prepare_mail($args['recipient'], $success_subject, $success_text, FALSE);

    }

    /**
     * Send final Data to Admin.
     * @var $params
     */
    public function send_form_to_admin( $params ) {

        $defaults = array(
            'html' => '',
            'template_placeholder_data' => FALSE
        );

        $args = wp_parse_args( $params, $defaults );

        $recipient = $this->structure_class->get_form_config('recipient', 'config' );
        $recipient_subject = $this->structure_class->get_form_config('form_name', 'config');
        $recipient_text = $this->structure_class->get_form_config('default_mail_text', 'config');

        $recipient_text = str_replace('%rendered_form_data%', nl2br( $args['html'] ), $recipient_text);
        $recipient_text = apply_filters('the_content', $recipient_text );

        $recipient_text = $this->structure_class->parse_config_template_placeholder( $recipient_text );

        if( $args['template_placeholder_data'] !== FALSE ) {

            $recipient_text = $this->structure_class->parse_default_template_placeholder($recipient_text, $args['template_placeholder_data']);

        }

        $recipient_text .= $this->structure_class->get_mail_footer_text();

        return $this->prepare_mail($recipient, $recipient_subject, $recipient_text, TRUE);

    }


    /**
     * Send an Copy to User, if valid mail is given.
     * @var $params array
     * @return bool
     */
    public function send_copy_to_user( $params ) {

        $defaults = array(
            'recipient' => NULL,
            'template_placeholder_data' => FALSE
        );

        $args = wp_parse_args( $params, $defaults );

        if( $args['recipient'] === NULL )
            return FALSE;

        $recipientSubject = $this->structure_class->get_form_config( 'copy_subject', 'config' );
        $confirmationText = $this->structure_class->get_form_config( 'copy_text', 'config' );

        $confirmationText = str_replace('%rendered_form_data%', nl2br( $args['html'] ), $confirmationText);
        $confirmationText = apply_filters('the_content', $confirmationText);

        $confirmationText = $this->structure_class->parse_default_template_placeholder( $confirmationText, $args['template_placeholder_data']);

        $recipientText  = $confirmationText;
        $recipientText .=  $this->structure_class->get_mail_footer_text();

        return $this->prepare_mail($args['recipient'], $recipientSubject, $recipientText, FALSE );

    }

    /**
     *
     * Send Mail with Dachcom Assistent
     * @param $recipient
     * @param $recipientSubject
     * @param $recipientText
     * @param bool $sendAttachment
     * @return bool
     */
    private function prepare_mail( $recipient, $recipientSubject, $recipientText, $sendAttachment = TRUE ) {

        $header = array();
        $html = TRUE;
        $attachments = FALSE;

        $has_attachments = $this->has_attachment() && $sendAttachment == TRUE;

        if( $has_attachments ) {

            //submit via mail
            if( $this->attachment_type == 'attachment') {

                $attachments = $this->attachment_data;

            //store on server
            } else if( $this->attachment_type == 'server_link' ) {

                $download_link  = $this->attachment['download_link'];
                $delete_link    = $this->attachment['delete_link'];

                //check if main_zip_folder exists
                //Alle Anhänge wurden auf den Server geladen und stehen als Archiv unter folgendem Link zum Download bereit
                $recipientText .= '<br><br>';
                $recipientText .= sprintf( __('all attachments has been stored on your server. to download the files click here: <a href="%s">%s</a>', 'dachcom-form-generator'), esc_url( $download_link ), esc_url( $download_link ) );

                //Um die Datei zu löschen</strong>, bitte diesen Link anwählen:
                $recipientText .= '<br><br><hr><br>';
                $recipientText .= sprintf( __('if you want to <strong>delete</strong> the archive, please open this link: <a href="%s">%s</a>', 'dachcom-form-generator'), esc_url( $delete_link ), esc_url( $delete_link ) );

            }

        }

        $mail_params = array(

            'recipient' => $recipient,
            'subject' => $recipientSubject,
            'message' => $recipientText,
            'copy' => FALSE,
            'attachments' => $attachments,
            'html' => $html,
            'header' => $header

        );

        $this->send_mail( $mail_params );

        return TRUE;

    }


    /**
     *
     * Send Mail with Dachcom Assistent
     *
     */
    public function send_mail( $params = array() ) {

        if( class_exists('Dachcom_Assistent_Manager') ) {

            $mail_params = $params;

            $assistent_manager = Dachcom_Assistent_Manager::factory();
            /** @var Dachcom_Assistent_Mail */
            $assistent_manager->call('mailAssistent')->send_mail( $mail_params );

        } else {

            //die. silently.
        }

        return TRUE;

    }


}