<?php

Class DachcomFormGenerator_Newsletter {

    var $structure_class;

    var $storage_class;

    /**
     * @var string email
     */
    var $user_mail_address = '';

    /**
     * Page ID of Page, which processes the Newsletter doi handling.
     * @var int
     */
    var $newsletter_landing_page_id = 0;

    /**
     * can be mixed, if no newsletter selection is available
     * @var mixed (array|null)
     */
    var $newsletter_elements = array();

    /**
     * If there is a DB Entry, set the ID here.
     * necessary to generate a DOI Link.
     * @var int
     */
    private $newsletter_stored_form_id = 0;

    /**
     * @var array
     */
    var $newsletter_selected_elements = array();

	function __construct( DachcomFormGenerator_Structure $structure_class ) {

        $this->structure_class = $structure_class;
        $this->storage_class = new DachcomFormBuilder_Storage();

        $this->newsletter_landing_page_id = $this->structure_class->get_form_config('newsletter_landingpage_id');

        $this->set_newsletter_elements( $this->structure_class->form_data['fields']['newsletter_field']['value'] );

	}

    public function set_user_mail_address( $address ) {

        $this->user_mail_address = $address;

    }

    public function set_newsletter_stored_form_id( $db_id ) {

        $this->newsletter_stored_form_id = $db_id;

    }

    public function set_selected_newsletter_elements( $elements  ) {

        $this->newsletter_selected_elements = $elements;

    }

    private function set_newsletter_elements( $elements  ) {

        $this->newsletter_elements = $elements;

    }

    public function generate_newsletter_list_url( $recipient ) {

        $activation_link = get_permalink( $this->newsletter_landing_page_id );

        return $activation_link . '?uid=' . base64_encode( $recipient );

    }

    /**
     * Parse Double Opt In Text
     * @return mixed|void
     */
	public function parse_doi_link( $form_id ) {

        $user_mail = $this->user_mail_address;
		$newsletter_landing_page_id = $this->newsletter_landing_page_id;

        if( $user_mail === FALSE)
            return '[no valid user mail]';

        if( empty( $newsletter_landing_page_id ) )
            return '[no valid landing page id]';

        $landing_page = get_post( $newsletter_landing_page_id );

        if( empty( $landing_page ) )
            return '[no valid landing page id]';


        //activation link
		$activation_link = get_permalink( $newsletter_landing_page_id) . '?type=' . strtolower( $form_id ) .'&dbID=' . $this->newsletter_stored_form_id . '&recipient=' . $user_mail;
        $activation_link = '<a href="' . $activation_link . '">' . $activation_link . '</a>';

		return $activation_link;

	}

    public function parse_user_newsletter_selection( $form_data ) {

        $user_newsletter_data =  $form_data->nl_data;

        $newsletter_data = array();
        $new_newsletter_data = array();

        //add a info to each newsletter checked
        if( !empty( $this->newsletter_selected_elements ) ) {

            foreach( (array) $this->newsletter_selected_elements as $fID => $registeredFormValue ) {

                $newsletter_data[ $registeredFormValue ] = array( 'slug' => $registeredFormValue, 'active' => 0, 'added' => date('Y-m-d H:i:s') );

            }

        }

        //there are stored newsletter infos. only add new selected ones.
        if( !empty( $user_newsletter_data ) ) {

            foreach($newsletter_data as $newFormValue => $newFormData ) {

                //add new if not avaiable
                if( !isset( $user_newsletter_data[ $newFormValue ] ) ) {

                    $new_newsletter_data[ $newFormValue ] =  array( 'slug' => $newFormValue, 'active' => 0, 'added' => date('Y-m-d H:i:s') );

                }

            }

        }

        $parsed_newsletter_data = array_merge( $newsletter_data, $new_newsletter_data);

        return $parsed_newsletter_data;

    }

    /**
     * Returns selected Newsletter, if available
     * @return string
     */
	public function parse_newsletter_selection( $newsletter_data ) {

		//do we have already some stored values?
		$stored_data = $newsletter_data;

		$sel_nl_str = '';

        $newsletters = $this->newsletter_elements;

        //we don't have a selection of newsletter, it's just a simple newsletter element.
        //return it.
        if( !is_array( $newsletters ) )
            return '';

        $selected_newsletters = $this->newsletter_selected_elements;

        foreach( (array) $selected_newsletters as $snl) {

			foreach( $newsletters as $nl) {

                //after storing, snl is an array with info.
                $snl_slug = is_array( $snl ) ? $snl['slug'] : $snl;

				$already_added = $stored_data !== FALSE && isset( $stored_data[ $snl_slug ] ) && $stored_data[ $snl_slug ]['active'] == 1;

				if( $nl['value'] == $snl_slug ) {

					if( $already_added )
                        $sel_nl_str .= '<i>';

                    $sel_nl_str .= '- ' . $nl['label'] . ' ' . ( $already_added ?  '(' . __('that newsletter is already activated', 'dachcom-form-generator') . ')' : '' );

					if( $already_added )
                        $sel_nl_str .= '</i>';

                    $sel_nl_str .= '<br>';

				}

			}

		}

        return $sel_nl_str;

	}



}