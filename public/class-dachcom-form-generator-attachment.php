<?php

Class DachcomFormBuilder_Attachment {


    /**
     * Holds the Temp Upload Folder
     * mostly WP_CONTENT_DIR / uploads / tmp
     * @var string
     */
    private $upload_dir = '';

    /**
     * The registered file data ($_FILES)
     * @var array
     */
    private $file_data = array();

    /**
     *
     * Stores all the Attachment Data in upload_dir
     * until its procecced by server download plugin or send as attachment via mail
     * @var array
     */
    private $attachment_data = array();

    private $max_file_size = 0;

    private $has_error = FALSE;

    private $error_message = '';

    function __construct() {

        $this->max_file_size = (3 * 1024 * 1024);

    }

    /**
     *
     * API
     *
     */


    public function set_file_data( $file_data ) {

        //if we have some file data, check file sizes.
        if( !empty( $file_data ) && isset( $file_data['attachment']['size']) ) {

            $sizes = (array) $file_data['attachment']['size'];
            $allSize = 0;

            foreach( $sizes as $size)
                $allSize += $size;

            if( $allSize > $this->max_file_size ) {

                $this->has_error = TRUE;
                $this->error_message = __('attachment files are to large (max file size:' . $this->max_file_size, 'dachcom-form-generator' );

            } else {

                $this->file_data = $file_data;

                $this->prepare_attachment();

            }

        }

        return $this->has_error;

    }


    public function is_valid() {

        return !$this->has_error;

    }

    public function has_attachment() {

        return !empty( $this->attachment_data );

    }

    public function get_error_message() {

        if( $this->has_error )
            return $this->error_message;

        return FALSE;

    }

    /**
     * Just returns the file paths (not links)
     * @return array
     */
    public function get_attachment_links() {

        return $this->attachment_data;

    }

    /**
     * @return array (download links and delete link
     */
    public function generate_download_link() {

        //check if main_zip_folder exists
        $zipFolder = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'linked-form-attachments';

        if( !is_dir( $zipFolder ) )
            mkdir( $zipFolder, 0755 );

        // Initialize archive object
        $zip = new FlxZipArchive;
        $token = basename( $this->upload_dir );

        $zipFileUrl = get_bloginfo('url') . '/formstore/' . $token . '.zip';
        $zipFileDeleteUrl = get_bloginfo('url') . '/formstore-delete/' . $token . '.zip';

        $zip->open( $zipFolder . DIRECTORY_SEPARATOR . $token . '.zip', ZipArchive::CREATE);

        $zip->addDir($this->upload_dir, $token);
        $zip->close();

        return array('download_link' => $zipFileUrl, 'delete_link' => $zipFileDeleteUrl );

    }

    public function clear() {

        $this->empty_upload_dir();

    }

    /**
     *
     *  Internal Methods
     *
     */

    private function prepare_attachment() {

        $attachments = array();

        if(isset($this->file_data['attachment']) && !empty($this->file_data['attachment'])) {

            $attachment = array_map(array($this, 'remap_files_array'),
                (array) $this->file_data['attachment']['name'],
                (array) $this->file_data['attachment']['type'],
                (array) $this->file_data['attachment']['tmp_name'],
                (array) $this->file_data['attachment']['error'],
                (array) $this->file_data['attachment']['size']

            );

            $upload_folder =    WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR;
            $tmp_folder =       $upload_folder . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;

            //create tmp folder
            if( !is_dir( $tmp_folder ) ) {

                mkdir( $tmp_folder, 0775 );

            }

            $this->upload_dir = $tmp_folder . uniqid('data-');

            if( !is_dir( $this->upload_dir ) ) {

                mkdir( $this->upload_dir, 0775 );

            }

            foreach( $attachment  as $file ) {

                $filename = $file['name'];
                $temp_name = $file['tmp_name'];

                $uploadPathFile = $this->upload_dir . DIRECTORY_SEPARATOR . $filename;

                move_uploaded_file($temp_name, $uploadPathFile );

                $attachments[] = $uploadPathFile;

            }

        }

        $this->attachment_data = $attachments;

    }

    private function remap_files_array($name, $type, $tmp_name, $error, $size) {

        return array(

            'name' => $name,
            'type' => $type,
            'tmp_name' => $tmp_name,
            'error' => $error,
            'size' => $size

        );

    }

    private function empty_upload_dir( $_dir = NULL ) {

        if( is_null( $_dir ) )
            $dir = $this->upload_dir;
        else
            $dir = $_dir;

        if ( is_dir( $dir )) {

            $objects = scandir( $dir );

            foreach ($objects as $object) {

                if ($object != "." && $object != "..") {

                    if (filetype($dir."/".$object) == "dir")
                        $this->clear( $dir ."/". $object );
                    else
                        unlink( $dir ."/". $object);
                }
            }

            reset($objects);
            rmdir($dir);

        }

    }

}
