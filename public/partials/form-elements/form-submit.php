<?php if( $show_default_response_layout && $form_type !== 'inline' ) { ?>

    <div id="<?php echo $form_id; ?>_logger" class="logger"></div>
    <div class="respond"></div>

<?php } ?>

<?php if( $formerror === TRUE ) { ?>

     <?php echo __('please fill out all required fields.', 'dachcom-form-generator'); ?>

<?php } else if( $mailerror === TRUE ) { ?>

    <?php echo  __('sorry, there was an error. please try it again later.', 'dachcom-form-generator'); ?>

<?php } ?>

<span class="buttonHolder">

    <input class="button btn btn-primary dachcom-form-generator-submit-ajax" type="submit" value="<?php echo $interface_submit_button; ?>" />

    <?php if( $show_reset_button ) { ?>

        <input class="resetForm button btn btn-default last" type="button" value="<?php echo $interface_reset_button; ?>" />

    <?php } ?>

</span>


<?php if( $show_default_response_layout && $form_type === 'inline' ) { ?>

    <div id="<?php echo $form_id; ?>_logger" class="logger"></div>
    <div class="respond"></div>

<?php } ?>