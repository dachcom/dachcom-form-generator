<?php

Class DachcomFormBuilder_Storage {

    var $_db_table_name = '';

	function __construct() {

        global $wpdb;

        $this->_db_table_name =  $wpdb->prefix . 'dcd_form_generator_data';

    }

    public function get_form( $id ) {

        global $wpdb;

        $query = $wpdb->prepare( "SELECT * FROM " . $this->_db_table_name . " WHERE id = %d LIMIT 1", $id );

        $formInfo = $wpdb->get_results( $query );

        if(!empty($formInfo) && isset($formInfo[0])) {

            $formInfo[0]->formdata = unserialize($formInfo[0]->formdata);

            if( !empty( $formInfo[0]->nl_data ))
                $formInfo[0]->nl_data = unserialize($formInfo[0]->nl_data);

            return $formInfo[0];

        } else {

            return FALSE;

        }

    }

    public function get_newsletter_form( $form_id, $recipient ) {

        global $wpdb;

        $query = $wpdb->prepare( "SELECT * FROM ".$this->_db_table_name." WHERE formID  = %s AND recipient = %s LIMIT 1", $form_id, $recipient );

        $formInfo = $wpdb->get_results( $query );

        if(!empty($formInfo) && isset($formInfo[0])) {

            $formInfo[0]->formdata = unserialize($formInfo[0]->formdata);

            if( !empty( $formInfo[0]->nl_data ))
                $formInfo[0]->nl_data = unserialize($formInfo[0]->nl_data);

            return $formInfo[0];

        } else {

            return FALSE;

        }

    }

    public function get_newsletter_form_data( $form_id, $recipient ) {

        $form_data = $this->get_newsletter_form( $form_id, $recipient );

        if( $form_data === FALSE )
            return FALSE;
        else
            return maybe_unserialize( $form_data->nl_data );

    }

    public function update_newsletter_form( $params ) {

        global $wpdb;

        $defaults = array(
            'id' => 0,
            'newsletter_data' => FALSE,
        );

        $args = wp_parse_args( $params, $defaults );

        $nl_data = $args['newsletter_data'];

        if( is_array( $nl_data ) )
            $nl_data = serialize( $nl_data );

        $wpdb->query( $wpdb->prepare(

            "UPDATE " .  $this->_db_table_name . "
					SET
					    nl_data = %s
					WHERE
					    id = %d",

            $nl_data,
            (int) $args['id']

        ) );

        return (int) $args['id'];

    }

    public function update_newsletter_state( $id, $newsletter_data ) {

        global $wpdb;

        $wpdb->query( "UPDATE " .  $this->_db_table_name . " SET nl_data = '" . serialize( $newsletter_data ) . "' WHERE id = " . $id );

        return TRUE;

    }

    public function insert_form(  $params ) {

        global $wpdb;

        $defaults = array(
            'id' => NULL,
            'form_id' => FALSE,
            'form_data' => FALSE,
            'user_mail' => FALSE
        );

        $args = wp_parse_args( $params, $defaults );

        $form_data = $args['form_data'];

        if( is_array( $args['form_data'] ) )
            $form_data = serialize( $args['form_data'] );

        if( is_null( $args['id'] ) ) {

            $wpdb->query(

                $wpdb->prepare(

                    "INSERT INTO " .  $this->_db_table_name . "
						( formID, recipient, formdata, stamp )
						VALUES ( %s, %s, %s, NOW() )",

                    $args['form_id'],
                    $args['user_mail'],
                    $form_data

                ));

            $id = $wpdb->insert_id;

        } else {

            $wpdb->query( $wpdb->prepare(

                "UPDATE " .  $this->_db_table_name . "
					SET
					    formdata = %s
					WHERE
					    id = %d",

                $form_data,
                (int) $args['id']

            ) );

            $id = $args['id'];

        }

        return $id;

    }

}