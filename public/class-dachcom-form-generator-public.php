<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dachcom_Form_Generator
 * @subpackage Dachcom_Form_Generator/public
 * @author     Dachcom Digital <shagspiel@dachcom.com>
 */
class Dachcom_Form_Generator_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dachcom_form_generator    The ID of this plugin.
	 */
	private $dachcom_form_generator;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;


	/**
	 * @var DachcomFormGenerator_Settings
	 */
	private $settings;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $dachcom_form_generator       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $dachcom_form_generator, $version ) {

		$this->dachcom_form_generator = $dachcom_form_generator;
		$this->version = $version;

		$this->settings = new DachcomFormGenerator_Settings();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->dachcom_form_generator, plugin_dir_url( __FILE__ ) . 'css/dachcom-form-generator-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		$forcejQ = TRUE;

		//is it jquery?
		if ($forcejQ || wp_script_is( 'jquery', 'enqueued' )) {

			if( !wp_script_is( 'validator', 'enqueued' ) ) {

				wp_enqueue_script( 'jquery-validator', plugin_dir_url( __FILE__ ) . 'js/validator.js', array(), $this->version, true );

				$tag = 'de';

				if( is_multisite() ) {

					$tag = $this->settings->get_current_languagecode();

				}

				if( $tag != 'en' && file_exists( plugin_dir_path( __FILE__ ) .'js/localization/messages_' . $tag. '.js' ) )
					wp_enqueue_script( 'jquery-validator-localization', plugin_dir_url( __FILE__ ) . 'js/localization/messages_' . $tag. '.js', array('jquery'), '1.11', true  );

			}

			if( !wp_script_is( 'jquery.form', 'enqueued' ) )
				wp_enqueue_script( 'jquery-form', plugin_dir_url( __FILE__ ) . 'js/jquery.form.js', array(), '3.32', true  );

			wp_enqueue_script( 'mrs-form-field-form-helper', plugin_dir_url( __FILE__ ) . 'js/script-jquery.js', array(), '1.0', true  );

			//is it beloved mootools?
		} elseif (wp_script_is( 'mootools-core', 'enqueued' ) || wp_script_is( 'mootools-core-1.4.5', 'enqueued' ) || wp_script_is( 'mootools-core-1.5', 'enqueued' ) ) {

			wp_enqueue_script( 'mrs-form-field-form-helper', plugin_dir_url( __FILE__ ) . 'js/script-mootools.js', array(), '1.0', true  );


		}


	}

}
