<?php

Class DachcomFormBuilder_Mapper {

    /**
     * @var DachcomFormGenerator_Structure
     */
    var $structure_class;

	/**
	 * stores all the fields (from post or db)
     * filled with (set form data
	 */
    private $fields;

	/**
     * add fields, which will not send to user
     */
	private $ignored_field_type = array('group-start', 'group_end', 'row-start', 'row-end', 'col-start', 'col-end', 'column', 'custom_html', 'image', 'password');


    private $has_error = FALSE;

    private $error_message = '';

    /**
     * All rendered Fields
     */
    private $rendered_form = '';

	function __construct( $structure_class ) {

        $this->structure_class = $structure_class;
	}

	public function set_data( $data ) {

        $this->fields = $data;

        unset( $this->fields['form_id'] );

        $this->parse_fields();

	}

    public function get_data() {

        return $this->fields;

    }

    public function get_rendered_form() {

        return $this->rendered_form;

    }

    public function is_valid() {

        return !$this->has_error;

    }

    public function get_error_message() {

        if( $this->has_error )
            return $this->error_message;

        return FALSE;

    }

	private function parse_fields() {

        if( !is_array( $this->fields ) )
            return FALSE;

		foreach($this->fields AS $type_key => $post_value ) {

            $_type = $this->prepare_post_type_element( $type_key );

            if( $_type === FALSE )
                continue;

            $type = $_type['type'];
            $sub_type = $_type['sub_type'];
            $type_info = $_type['type_info'];

            $title = isset( $type_info['title'] ) && !empty( $type_info['title'] )  ? $type_info['title'] : $type_key;

            /**
             * Repeater fields
             */
            if( $type == 'repeater' ) {

                $title = '<br><strong>' . $post_value . ':</strong><br>';

                //add data to global vars and continue
                $this->rendered_form .= $title;
                continue;

            /**
             * Checkboxes
             */
            } else if( $type == 'input' && $sub_type == 'checkbox' ) {

                $title = isset( $type_info['title'] ) ? $type_info['title'] : __('selection', 'dachcom-form-generator');

                //could be multiple, force to be.
                $post_value = (array) $post_value;
                $value = array();

                foreach( $post_value as $post_sub_key => $post_sub_value ) {

                    $t_label = isset( $type_info['value'][ $post_sub_key ]['label'] ) ? $type_info['value'][ $post_sub_key ]['label'] : $post_sub_key;
                    $t_value = isset( $type_info['value'][ $post_sub_key ]['label'] ) ? $type_info['value'][ $post_sub_key ]['label'] : 'yes';

                    $value[] = $t_label;

                }

                if( empty( $value ) )
                    $value =  __('no selection', 'dachcom-form-generator');
                else
                    $value = implode(', ', array_map('trim', $value ) );

            /**
             * Radio
             */
            } else if( $type == 'input' && $sub_type == 'radio' ) {

                $title = isset( $type_info['title'] ) ? $type_info['title'] : __('selection', 'dachcom-form-generator');

                //could be multiple, force to be. (radio: really??)
                $post_value = (array) $post_value;
                $value = array();

                $type_values = $type_info['value'];

                foreach( $post_value as $pval ) {

                    foreach( $type_values as $v ) {

                        if( $v['value'] == $pval ) {

                            $value[] = !empty( $v['label']) ? $v['label'] : 'no key';

                        }

                    }

                }

                if( empty( $value ) )
                    $value =  __('no selection', 'dachcom-form-generator');
                else
                    $value = implode(', ', array_map('trim', $value ) );

            /**
             * Select
             */
            } else if ( $type == 'select' ) {

                $title = isset( $type_info['title'] ) ? $type_info['title'] : __('selection', 'dachcom-form-generator');

                $type_values = (array) $type_info['value'];

                //could be multiple, force to be.
                $post_value = (array) $post_value;
                $value = array();

                foreach( $post_value as $post_val) {

                    foreach( $type_values as $sval_key => $sval_value) {

                        //its grouped, check subfields
                        if( is_array( $sval_value ) ) {

                            foreach( $sval_value as $sub_value_key => $sub_value ) {

                                if($sub_value_key == $post_val) {
                                    $value[] = $sub_value;
                                }

                            }

                        } else {

                            if($sval_key == $post_val) {
                                $value[] = $sval_value;
                            }

                        }

                    }

                }

                //not found...
                if( empty( $value ) || $value == 'null' )
                    $value =  __('no selection', 'dachcom-form-generator');
                else
                    $value = implode(', ', array_map('trim', $value ) );

            /**
             * Default
             */
            } else {

                $value = (string) $post_value;

            }

            if(empty( $value ) && isset( $type_info['required'] ) && $type_info['required'] ) {

                $this->has_error = TRUE;

			} else {

				$title          = $this->structure_class->get_form_element($type_key . '_title_mail', $title);
				$title_prefix   = $this->structure_class->get_form_element($type_key . '_title_mail_prefix', '');
				$title_suffix   = $this->structure_class->get_form_element($type_key . '_title_mail_suffix', '<br>');

                //add maybe prefix and suffix
                if( isset( $type_info['prefix'] ) && !empty( $type_info['prefix'] ) )
                    $value = $type_info['prefix'] . ' ' . $value;

                if( isset( $type_info['suffix'] ) && !empty( $type_info['suffix'] ) )
                    $value =  $value . ' ' . $type_info['suffix'];

                $title = sanitize_text_field( $title );
                $value = sanitize_text_field( $value );

                $this->rendered_form .= $title . ': ' . $title_prefix . $value . $title_suffix;

			}

		}

	}

    private function prepare_post_type_element( $type_key ) {

        $type = null;
        $sub_type = null;
        $form_field_info = array();

        $is_repeater = FALSE;

        //we need to check if field is a repeater field.
        if( 1 == preg_match('/^repeater_field_(\d{1,3})$/', $type_key ) ) {

            $type = 'repeater';
            $is_repeater = TRUE;

        } elseif( 1 == preg_match('/^repeater_field_(\d{1,3})_+/', $type_key, $matches) ) {

            $sub_key = str_replace($matches[0], '', $type_key);
            $type = $this->structure_class->form_data['fields'][$sub_key]['type'];
            $form_field_info = $this->structure_class->form_data['fields'][$sub_key];
            $is_repeater = TRUE;

        } else {

            $form_field_info = $this->structure_class->form_data['fields'][ $type_key ];

            $type = $form_field_info['type'];
            $sub_type = isset( $form_field_info['sub_type'] ) ? $form_field_info['sub_type'] : null;

        }

        if( $is_repeater == FALSE && !isset( $this->structure_class->form_data['fields'][ $type_key ] ) )
            return FALSE;

        if( in_array( $type, $this->ignored_field_type ) )
            return FALSE;

        return array('type' => $type, 'sub_type' => $sub_type, 'type_info' => $form_field_info );

    }

}