<?php

class DachcomFormGenerator_Builder extends DachcomFormGenerator_Structure {

    /**
     *
     * Set FormType ( inline )
     * render will submit that info to single input render field
     * @var string
     */
    private $form_type = '';

	/**
	 * Stores all the fields to reach them again in a repeater case
	 * @var array
	 */
	private $_rendered_single_fields = array();

    private $_submit_block_printed = FALSE;

    private $_required_block_printed = FALSE;

	public function init( $form_id, $params = array() ) {

		parent::set_form( $form_id, $params);

	}

	/**
	 * 	use this method to update field value
	@deprecated
	 */
	public function update_field_value( $field_name, $value ) {

		$this->form_data['fields'][ $field_name ]['values'] = $value;

	}

	function render( ) {

		/* if there is no recipient defined in the admin area, return */
		$deactivate_mail_send = isset( $this->get_form_config['deactivate_mail_send'] ) ? $this->get_form_config['deactivate_mail_send'] : FALSE;

		if( $deactivate_mail_send === TRUE && ( !isset( $this->get_form_config['recipient'] ) || empty($this->get_form_config['recipient']->form_value) ) ) {

			if ( is_user_logged_in() ) {

                $message = 'Dieses Formular wird nicht dargestellt, da keine Empfänger-Adresse für dieses Formular im Admin-Bereich definiert wurde!';

				return $this->_get_view('form-error', array( 'message' => $message ) );

			}

		}

		return $this->_create_form( );

	}

	private function _create_form( ) {

		$str = '';

        $enctype =  $this->get_form_config('enctype', 'config');
        $this->form_type =  $this->get_form_config('form_type', 'config');

		if( empty( $enctype ) )
			$enctype = 'enctype="multipart/form-data"';

        $form_type_class = '';

        if( !empty( $this->form_type ) )
            $form_type_class = 'form-' . $this->form_type;

        $header_params =  array(

            'enctype' => $enctype,
            'form_id' => $this->form_id,
            'form_class' => $form_type_class,
            'method' => 'POST'
        );

		$str .= $this->_get_view('form-header', $header_params );

		$c = 0;
		$currentColumnID = 1;

		$columnCounter = $this->_count_columns( $this->form_data['fields'] );

		/**
		 * cycle through all field blocks
		 */
		foreach( $this->form_data['fields'] AS $fieldName => $fieldData ) {

			// setup globals
            $type =	 $fieldData['type'];

			//set title
			$title = isset($fieldData['title']) ? $fieldData['title'] : '';

			//set id
			$element_id = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;
			$element_id_html = 'id="' . $element_id . '"';

			if($c == 0 && $columnCounter == 0) {

				//setup first column, if no column setted in layout.
				$str .= '<div class="default-column">';

			}

            /*
             * GROUP & BLOCK VIEW ELEMENTS
            */
			if($fieldData['type'] == 'submit_block' ) {

                $str .= $this->_get_submit_block();

            } else if($fieldData['type'] == 'required_block') {

                $str .= $this->_get_required_block();

			} else if($fieldData['type'] == 'group-start') {

				$groupClass = isset( $fieldData['class'] ) ? $fieldData['class'] : '';
				$visibility = isset( $fieldData['visibility'] ) ? $fieldData['visibility'] : 'invisible';

				$str .= '<div '.$element_id_html.' class="group '. $groupClass .' ' . $visibility. '">';
				continue;

			} else if($fieldData['type'] == 'group-end') {

				//nothing to do exept closing the group.
				$str .= '</div>';
				continue;

            } else if($fieldData['type'] == 'row-start') {

                $rowClass = isset( $fieldData['class'] ) ? $fieldData['class'] : '';

                $str .= '<div class="row sub-row '. $rowClass . '">';
                continue;

            } else if($fieldData['type'] == 'row-end') {

                //nothing to do exept closing the row.
                $str .= '</div>';
                continue;

            } else if($fieldData['type'] == 'col-start') {

                $colClass = isset( $fieldData['class'] ) ? $fieldData['class'] : 'col-xs-12';

                $str .= '<div class="sub-col '. $colClass . '">';
                continue;

            } else if($fieldData['type'] == 'col-end') {

                //nothing to do exept closing the col.
                $str .= '</div>';
                continue;


            } else if($fieldData['type'] == 'column') {

				if( $currentColumnID > 1 )
					$str .= '</div>';

				$columnClass = isset( $fieldData['class'] ) ? $fieldData['class'] : '';

				$str .= '<div '.$element_id_html.' class="column '. $columnClass .'">';

				$currentColumnID++;

				continue;

            /*
             * HTML INPUT ELEMENTS
            */
			} else if($type == 'input') {

				$formFieldData = $fieldData;
				$formFieldData['id'] = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;

				$el_html = $this->_get_element_block( $fieldName, $formFieldData );

				$this->_rendered_single_fields[ $fieldName ] = $el_html;

				$str .= $el_html;

			} else if($type == 'hidden') {

                $formFieldData = $fieldData;
                $formFieldData['id'] = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;

                $el_html = $this->_get_element_block( $fieldName, $formFieldData );

                $this->_rendered_single_fields[ $fieldName ] = $el_html;

                $str .= $el_html;

			} elseif($type == 'select') {

				$formFieldData = $fieldData;
				$formFieldData['id'] = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;

				$el_html = $this->_get_element_block( $fieldName, $formFieldData );

				$this->_rendered_single_fields[ $fieldName ] = $el_html;

				$str .= $el_html;

			} elseif( $type == 'checkbox' ) {

				$formFieldData = $fieldData;
				$formFieldData['id'] = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;

				$el_html = $this->_get_element_block( $fieldName, $formFieldData );

				$this->_rendered_single_fields[ $fieldName ] = $el_html;

				$str .= $el_html;

			} elseif($type == 'radio') {

                $formFieldData = $fieldData;
                $formFieldData['id'] = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;

                $el_html = $this->_get_element_block( $fieldName, $formFieldData );

                $this->_rendered_single_fields[ $fieldName ] = $el_html;

                $str .= $el_html;


			} elseif($type == 'textarea') {

                $formFieldData = $fieldData;
                $formFieldData['id'] = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;

                $el_html = $this->_get_element_block( $fieldName, $formFieldData );

                $this->_rendered_single_fields[ $fieldName ] = $el_html;

                $str .= $el_html;

			} elseif( $type == 'file' ) {

                $formFieldData = $fieldData;
                $formFieldData['id'] = isset($fieldData['id']) ? $fieldData['id'] : $this->form_id . '-' . $fieldName;

                $el_html = $this->_get_element_block( $fieldName, $formFieldData );

                $this->_rendered_single_fields[ $fieldName ] = $el_html;

                $str .= $el_html;

            /*
             * HTML VIEW ELEMENTS
            */
			} elseif($type == 'seperator') {

				$html = '';

				if( !empty($title) ) {

					$html.= '<label for="'.$fieldName.'" >'.$title.'</label>';

				}

				$mode =	 isset( $fieldData['mode'] ) ? $fieldData['mode'] : '';

				$html.= '<div class="seperator '. $mode .'"></div>';

				$this->_rendered_single_fields[ $fieldName ] = $html;

				$str .= $html;

            } elseif($type == 'custom_html') {

                $html = '';

                $text = $fieldData['value'];

                $html.= '<span class="custom-html">'.$text.'</span>';

                $this->_rendered_single_fields[ $fieldName ] = $html;

                $str .= $html;

            } elseif($type == 'headline') {

                $html = '';
                $text = $fieldData['value'];
                $size = $fieldData['size'];

                $html.= '<span class="headline"><' . $size . '>' . $text . '</' . $size . '></span>';

                $this->_rendered_single_fields[ $fieldName ] = $html;

                $str .= $html;

            } elseif($type == 'image') {

				$mediaSize = $fieldData['size'];
                $attachment_id = $fieldData['image_id'];

				$caption = !empty($fieldData['title']) ? $fieldData['title'] : '';

                $post_thumbnail_id = wp_get_attachment_image( $attachment_id, $mediaSize );

                $html = '';

				if( !empty ( $post_thumbnail_id ) ) {

                    if( $caption !== FALSE ) {

                        $caption = get_post_field('post_excerpt', $attachment_id);

                    }

                    $html .= '<span class="content-media">';
                        $html .= '<span class="image">' . $post_thumbnail_id . '</span>';
                        $html .= '<span class="caption">'.$caption.'</span>';
                    $html .= '</span>';

				}

				$this->_rendered_single_fields[ $fieldName ] = $html;

				$str .= $html;

			} elseif($type == 'repeater') {

				$fields = $fieldData['fields'];
				$max_repeater = isset($fieldData['max-repeater-fields']) ? $fieldData['max-repeater-fields'] : -1;
				$fieldTitle = $fieldData['title'];

				if( $max_repeater > 0 )
					$fieldTitle = $fieldTitle. ' <span class="view-repeater-counter">('. $max_repeater .')</span';

				$_notAllowedRepeaterFields = array( 'column','row-start', 'row-end', 'col-start', 'col-end','image' );

				$html  = '<div class="seperator"></div>';
				$html .= '<a href="#" class="repeat-button add" data-repeater-id="'.$fieldName.'" data-max-fields="' . $max_repeater. '">' . $fieldTitle . '</a>';
				$html .= '<div class="repeater-data clone">';

				$html .= '<input type="hidden" data-name="repeater_field" value="'.$fieldName.'">';

				$html .= '<span class="block-title subHeadline">' . $fieldData['blocktitle'] . '</span>';
				$html .= '<span class="align-right subHeadline"><a href="#" class="remove-block">' . __('remove block', 'dachcom-form-generator') . '</a></span>';

				foreach( $fields as $field) {

					if( in_array( $field, $_notAllowedRepeaterFields ))
						continue;

					if( isset( $this->_rendered_single_fields[ $field ])) {

						$html .= '<div class="clone-item">';

                            $fieldHTML =  $this->_rendered_single_fields[ $field ];
                            $fieldHTML =  str_replace('name="', 'data-name="', $fieldHTML);

                            $html .= $fieldHTML;

						$html .= '</div>';

					}

				}

				$html .= '</div>';

				$str .= $html;

			}

			$c++;

		}

        //fallbacks, if elements where not setted.
        $str .= $this->_get_required_block();
        $str .= $this->_get_submit_block();

		$str .= '</div>'; //close last column .column

		$str .= $this->_get_view('form-footer', array( 'formID' => $this->form_id ) );

		return $str;

	}

	private function _count_columns( $fields ) {

		$c = 0;

		foreach( $fields AS $fieldData ) {

			if($fieldData['type'] == 'column') {
				$c++;

			}

		}

		return $c;

	}


	/**
	 * @param $type
	 * @param $params
	 * @return string
	 */
	private function _get_element_block( $el_name, $el_params ) {

		$element_type = ucfirst( $el_params['type'] );
		$sub_type = NULL;

		if( isset( $el_params['sub_type']) )
			$sub_type = '_' . ucfirst( $el_params['sub_type'] );

		$formElementClass = 'FormElement_' . $element_type . $sub_type;

		/** @var $el FormElement */
		$el = new $formElementClass( $el_name, $el_params, $this->form_type );
		$el->set_required_symbol($this->get_form_config('required_symbol', 'config') );

		return  $el->get_form_layout();

	}

    /**
     * returns the submit block
     */
    private function _get_submit_block( ) {

        if( $this->_submit_block_printed )
            return '';

        $params = array(

            'form_id'						=> $this->form_id,
            'form_type'						=> $this->form_type,
            'show_default_response_layout' 	=> $this->get_form_config('show_response_field', 'config' ),
            'show_reset_button'				=> $this->get_form_config('show_reset_button', 'config' ),

            'interface_submit_button'		=> $this->get_form_config('interface_submit_button', 'config'),
            'interface_reset_button'		=> $this->get_form_config('interface_reset_button', 'config'),

            'formerror'						=> isset( $_GET['formerror'] ),
            'mailerror' 					=> isset( $_GET['mailerror'] ),

        );

        $this->_submit_block_printed = TRUE;

        return $this->_get_view('form-submit', $params);

    }

    /**
     * returns the HTML Block for the required text.
     * only returns string, if required_text is setted
     */
    private function _get_required_block() {

        $required_text = $this->get_form_config('required_text', 'config');

        if( $this->_required_block_printed || empty( $required_text ) )
            return '';


        $this->_required_block_printed = TRUE;

        return $this->_get_view('form-required', array( 'required_text' => $required_text ) );

    }

	/**
	 * @param $view_element
	 * @param array $params
	 * @return string
	 */
	private function _get_view( $view_element, array $params = array() ) {

		ob_start();

		//extract everything in param into the current scope
		$params['this'] = $this;
		extract($params, EXTR_SKIP);

		require plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/form-elements/' . $view_element . '.php';

		$html = ob_get_clean();

		return $html;

	}

}