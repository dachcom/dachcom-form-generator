<?php

/**
 * Send Mails, Attachments, read and parse. Olè...
 * @author shagspiel@dachcom.ch
 *
 *	filter:
 *	- add_filter('dachcom-form-generator/setup-post-data');
 *	- add_filter('dachcom-form-generator/extend-request-json-args');
 *
 */
Class DachcomFormBuilder_Ajax {

    /**
     * Check, if incoming is an ajax request
     * @var bool
     */
	var $is_ajax = FALSE;

    /**
     * Set Refer URL of Form
     * @var string
     */
	var $refer_url = '';

    /**
     * Store Data in Database
     * @var bool
     */
	var $store_in_db = FALSE;

    /**
     * Check if Form is a Newsletter form.
     * @var bool
     */
	var $is_newsletter = FALSE;


    /**
     * Check if need to send a copy to User.
     * @var bool
     */
	var $send_copy_to_user = FALSE;

    /**
     * All rendered Fields
     */
    var $rendered_form = '';

    /**
     * @var DachcomFormGenerator_Structure
     */
    var $structure_class = null;

    /**
     * @var DachcomFormGenerator_Newsletter
     */
    var $newsletter_class = null;

    /**
     * @var DachcomFormBuilder_Mailer
     */
    var $mailer_class = null;

    /**
     * @var DachcomFormBuilder_Attachment
     */
    var $attachment_class = null;

    /**
     * @var DachcomFormBuilder_Mapper
     */
    var $mapper_class = null;

    /**
     * @var DachcomFormBuilder_Storage
     */
    var $storage_class = null;

	function __construct() {

		add_action('wp_ajax_nopriv_dachcom_form_generator_ajax', array($this, 'init') );
		add_action('wp_ajax_dachcom_form_generator_ajax',  array($this, 'init') );

	}

	function init() {

        $this->set_default_settings();

        $this->set_post_data( $_POST );
		$this->set_file_data( $_FILES );

        $this->save();

        $this->send();

        $this->output();

	}

    function set_default_settings() {

        $this->structure_class = new DachcomFormGenerator_Structure();
        $this->structure_class->set_form( $_POST['form_id'] );

        $this->mailer_class = new DachcomFormBuilder_Mailer( $this->structure_class );

        $save_data_in_db             = $this->structure_class->get_form_config('save_data_in_db', 'config');
        $is_newsletter               = $this->structure_class->get_form_config('is_newsletter', 'config');
        $send_copy_to_user           = $this->structure_class->get_form_config('send_copy_to_user', 'config');

        if( isset( $is_newsletter ) && isset( $_POST['newsletter_field'] ) ) {

            $this->is_newsletter = TRUE;
            $this->_setup_newsletter_class( $_POST['newsletter_field'] );

        }

        if( ( isset( $save_data_in_db ) && $save_data_in_db == TRUE ) || $this->is_newsletter ) {

            $this->storage_class = new DachcomFormBuilder_Storage();
            $this->store_in_db = TRUE;

        }

        if( isset( $send_copy_to_user ) && $send_copy_to_user == TRUE )
            $this->send_copy_to_user = TRUE;

        $this->is_ajax =    isset( $_POST['isAjax']) ? TRUE : FALSE;
        $this->refer_url =  isset( $_POST['ref'] ) ? $_POST['ref'] : '';

        unset( $_POST['isAjax'], $_POST['ref'] );

        return TRUE;

    }

    function set_post_data( $post_data ) {

        /**
         * no valid post data
         */
		if( empty( $post_data ) ) {

            wp_die('no valid $_POST data');

        }

        /**
         * honeypot triggered => Spam!
         */
		if( !empty( $post_data['addition_name']) ) {

            wp_die('You\'re not human. That\'s bad. Very bad.');

        }

        /**
         * remove core fields
         */
		unset( $post_data['addition_name'], $post_data['action'] );

        $this->mapper_class = new DachcomFormBuilder_Mapper( $this->structure_class );
        $this->mapper_class->set_data( apply_filters('dachcom-form-generator/setup-post-data', $post_data ) );

        if( !$this->mapper_class->is_valid() ) {

            $this->_throw_error( $this->mapper_class->get_error_message() );

        }

	}

	function set_file_data( $file_data ) {

        $this->attachment_class = new DachcomFormBuilder_Attachment();
        $this->attachment_class->set_file_data( $file_data );

        if( !$this->attachment_class->is_valid() ) {

            $this->_throw_error( $this->attachment_class->get_error_message() );

        }

        if( $this->attachment_class->has_attachment() ) {

            $fm_type =  $this->structure_class->get_form_config('file_management');

            if( $fm_type === FALSE)
                $fm_type = 'attachment';

            if( $fm_type == 'attachment') {

                $this->mailer_class->add_attachment( $this->attachment_class->get_attachment_links(), 'attachment' );

            } else if( $fm_type == 'server_link') {

                $this->mailer_class->add_attachment( $this->attachment_class->generate_download_link(), 'server_link' );

            }

        }

	}

	function send() {

        if( isset( $this->structure_class->form_data['configurations']['send_mail'])
            && $this->structure_class['configurations']['send_mail'] === FALSE  )
            return false;

        //its a newsletter form. we do not need to send an mail to admin, because we're awaiting an activation link
		if( $this->is_newsletter ) {

            $newsletter_data = $this->storage_class->get_newsletter_form_data( $this->structure_class->form_id, $this->extract_user_mail_from_form() );

            $params = array(
                'activation_link'        => $this->newsletter_class->parse_doi_link( $this->structure_class->form_id ),
                'selected_newsletters'   => $this->newsletter_class->parse_newsletter_selection( $newsletter_data ),
                'recipient'              => $this->extract_user_mail_from_form()
            );

			$this->mailer_class->send_activation_mail( $params );

		} else {

            $params = array(
                'html'                      => $this->mapper_class->get_rendered_form(),
                'template_placeholder_data' => $this->mapper_class->get_data()
            );

            $this->mailer_class->send_form_to_admin( $params );

			if( $this->send_copy_to_user ) {

                $params = array(
                    'recipient' => $this->extract_user_mail_from_form(),
                    'template_placeholder_data' => $this->mapper_class->get_data()
                );

				$this->mailer_class->send_copy_to_user( $params );

			}

		}

        //garbage collecting
        $this->attachment_class->clear();

	}

	private function save() {

		if( !$this->store_in_db )
			return FALSE;

		if( is_multisite() )
			switch_to_blog( 1 );

        $update_id = NULL;

        $newsletter_data = NULL;

        $params = array(
            'id' => $update_id,
            'form_id' => $this->structure_class->form_id,
            'form_data' => $this->mapper_class->get_data(),
            'user_mail' => $this->extract_user_mail_from_form()
        );


        $this->storage_class->insert_form( $params );

        /**
         *
         * If it's a Newsletter, user maybe wants to update his data.
         * Check that here.
         *
         */
        if( $this->is_newsletter ) {

            $newsletter_data = $this->storage_class->get_newsletter_form( $this->structure_class->form_id, $this->extract_user_mail_from_form() );

            if( $newsletter_data !== FALSE ) {

                $update_id = $newsletter_data->id;

                $params = array(
                    'id' => $update_id,
                    'newsletter_data' => $this->newsletter_class->parse_user_newsletter_selection( $newsletter_data ),
                );

                $this->storage_class->update_newsletter_form( $params );

                //inform newsletter class about the stored db entry
                $this->newsletter_class->set_newsletter_stored_form_id( $update_id );

            }

        }

		if( is_multisite() )
			restore_current_blog();

	}

	function output() {

		global $mrs_mail_shredder;

		$message = $this->structure_class->get_form_config('text_on_success', 'config');

        //first, default ones
        $message = str_replace('%rendered_form_data%', nl2br( $this->mapper_class->get_rendered_form() ), $message);
        $message = $this->structure_class->parse_default_template_placeholder($message, $this->mapper_class->get_data() );

		remove_filter('the_content', array($mrs_mail_shredder, 'shredder_mail') );

		$message = apply_filters('the_content', $message, false);

		add_filter('the_content', array($mrs_mail_shredder, 'shredder_mail'));

		if( !$this->is_ajax ) {

			wp_redirect($this->refer_url);
			exit;

		} else {

			header('Cache-Control: no-cache, must-revalidate');
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: text/plain; charset=utf-8');

			$json_args = array('status' => 'ok', 'message' => NULL, 'message' => $message );

			$json_args = apply_filters('dachcom-form-generator/extend-request-json-args', $json_args, $this->structure_class->form_id, $this->mapper_class->get_data() );

			echo json_encode( $json_args );
			exit;

		}

	}

    private function _setup_newsletter_class( $newsletter_field_selection ) {

        $user_mail = $this->extract_user_mail_from_form();

        $this->newsletter_class = new DachcomFormGenerator_Newsletter( $this->structure_class );

        $this->newsletter_class->set_user_mail_address( $user_mail );
        $this->newsletter_class->set_selected_newsletter_elements( $newsletter_field_selection );

    }

    function extract_user_mail_from_form() {

        $mail = '';

        if( isset( $_POST['mail']) )
            $mail = $_POST['mail'];
        elseif( isset( $_POST['email']) )
            $mail = $_POST['email'];
        elseif( isset( $_POST['mail_address']) )
            $mail = $_POST['mail_address'];

        $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);

        if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            return $mail;
        }

        return FALSE;

    }

    private function _throw_error( $message ) {

        header("HTTP/1.0 500 Internal Server Error");

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: text/plain; charset=utf-8');

        echo json_encode( array('status' => 500, 'message' => $message ) );

        exit;

    }
}