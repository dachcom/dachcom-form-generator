<?php

/**
 *
 * Public Functions
 *
 */

function dfg_render_form( $form_name, array $params = array() ) {

	$formBuilder = new DachcomFormGenerator_Builder();
	$formBuilder->init( $form_name, $params );

	return $formBuilder->render();

}

function dfg_get_form_config() {

	$settings = new DachcomFormGenerator_Settings();

	return $settings->get_config_form();

}
