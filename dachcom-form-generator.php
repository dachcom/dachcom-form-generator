<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.dachcom.com
 * @since             1.0.0
 * @package           Dachcom_Form_Generator
 *
 * @wordpress-plugin
 * Plugin Name:       Dachcom Form Generator
 * Plugin URI:        repro.dachcom.com
 * Description:       Generates beautiful Forms.
 * Version:           2.3.3
 * Author:            Dachcom Digital
 * Author URI:        http://www.dachcom.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dachcom-form-generator
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dachcom-form-generator-activator.php
 */
function activate_dachcom_form_generator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-form-generator-activator.php';
	Dachcom_Form_Generator_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dachcom-form-generator-deactivator.php
 */
function deactivate_dachcom_form_generator() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-form-generator-deactivator.php';
	Dachcom_Form_Generator_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dachcom_form_generator' );
register_deactivation_hook( __FILE__, 'deactivate_dachcom_form_generator' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dachcom-form-generator.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dachcom_form_generator() {

	$plugin = new Dachcom_Form_Generator();
	$plugin->run();

}
run_dachcom_form_generator();
