=== Plugin Name ===
Plugin Name: Dachcom Form Generator

== Description ==


== Installation ==

1. Upload to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. You need a updated form.php in your theme and some .htaccess tweaks. use recent wp .htaccess.

== Changelog ==
= 2.3.3=
* Themosis / Default Theme Abstraction
* Bugfixes
= 2.3.2=
* Themosis FW 1.2 ready
= 2.3.1 =
* Small Code Fixes
= 2.3.0 =
* Add Selector for required field. usage: <code>'required_block' => array('type' => 'required_block');</code>
= 2.2.0 =
* Added input fields
* Added row and col support
* Added addon to input fields
= 2.1.0 =
* Form Improvements
= 2.0.0 =
* wordpress plugin boilerplate interated
* dachcom engine integrated
= 1.7.2 =
* added placeholder support
* bug fixed for language helper
= 1.7.1 =
- fixed json header on windows
= 1.7.0 =
- added radio input support for target toggler.
= 1.6.9 =
- added localization support for jquery
= 1.6.8= 
- added mootools js suppoert
- role bugfixed, removed "access_network" caps added "mange_options" caps
- fixed builde, "ref" Post Value is not required anymore.
= 1.6.6= 
- bugfix in request_handler.php
= 1.6.5= 
- div. bugs
= 1.6.4= 
- succes header type set to json   
= 1.6.3= 
- init dropdown value was changed from "null" to "", in order to work properly with the validation-plugin  
= 1.6.2=
- checkboxes/radiobuttons do now have the abilities to display a description (i.e.: Agreement-Text )
= 1.6.1=
- "submit_block" was renderd inside of ".blockitem" (added to array "$disallowed_form_groups")
= 1.6.0=
- added "deactivate_mail_send" for usage in forms.php (disable send mail on purpose)
= 1.5.9=
- added postdata to filter "mrs/extend_request_json_args"
= 1.5.8=
* - now the plugin also works for requested forms ( with jquery's ".on()")
* - added filter "mrs/extend_request_json_args" to extend the json_args for the request_handler.php
= 1.5.7=
* removed js error
= 1.5.6=
* send_mail() to globalize send mails in form
* updated missing lang sections
* added file_management (as attachemnt or as zip archive with downloadlink)
* all non-html tags like "\n" has been removed
= 1.5.5=
* fixed some logical errors
* added request handler to submit form into plugin stage
= 1.5.1=
* fixed template error, if config not available.
= 1.5 = 
* check if mrs_plugin class exists.
= 1.0.1 =
* add class-declaration for "column-field" like:
'column' 		=> array('type' => 'column', 'class' => 'classY classZ'),
= 1.0 =
* A change since the previous version.


== ToDo ==
* Add Newsletter System ( In Progress )
* Rebuild Server | Attachment-System (Links, Mail)
* Fix Radio Toggle Groups